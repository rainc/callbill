var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var log4js = require('log4js');
var routers = require('./routers');

log4js.configure({
    appenders: [
        { type: 'console' }, //����̨���
        {
            type: 'dateFile', //��־�ļ����
            filename: config.webLogFile,
            //     maxLogSize: 1024,
            backups: 3,
            pattern: "-yyyy-MM-dd",
            alwaysIncludePattern: false,
            category: 'WEBSERVER'
        }
    ],
    replaceConsole: true
});
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false ,limit: '50mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'callBill', // ����ʹ�� 128 ���ַ�������ַ���
    cookie: { maxAge: 6000 * 1000 }
}));

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

//路由
routers(app);
//默认初始语言为中文
localLan = 'zh_CN';

module.exports = app;
