/**
 * Created by Rainc on 2015/8/28.
 */

var api = require('./controllers/api');

module.exports = function (app) {
  app.get(['/','/bootstrap'],function(req,res){
    if(req.session.isVisit) {
      res.render('bootstrap.html');
    } else {
      res.render('login.html');
    }
  });
  app.get('/login',function(req,res){
    res.render('login.html');
  })

  app.get('/api',api.callBillApi);
  app.post('/api',api.callBillApi);
}