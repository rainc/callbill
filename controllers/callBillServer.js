/**
 * Created by Rainc on 2015/9/8.
 */
var mysql1 = require('./basedao.js');
var fs = require('fs');
var async = require('async');
var radius = require('radius');
var global = require('./global.js').global;
var log4js = require('log4js');

log4js.configure({
  appenders: [
    { type: 'console' }, //控制台输出
    {
      type: 'dateFile', //日志文件输出
      filename: config.serverLogFile,
      //     maxLogSize: 1024,
      backups: 3,
      pattern: "-yyyy-MM-dd",
      alwaysIncludePattern: false,
      category: 'WEBSERVER'
    }
  ],
  replaceConsole: true
});
var logger = log4js.getLogger('CALLBILLSERVER');
logger.setLevel('INFO');

var callBillDir = './callBillDir';
domain = [];
mtgStatistics = [];

getRequestNum = 0; //收到话单写入请求数
writeSuccessNum = 0; //成功写入数
lastingTotalDelay = 0; //写入持久存储区总延时

exports.init = function(){
  try{
    //定时检查callBillDir目录下的文件，若有，则读取后入库
    setInterval(function(){
      fs.readdir(callBillDir,function(err,files){
        if(err){
          logger.error(err);
        }else{
          async.forEachSeries(files,function(item,cb){
            var filePath = callBillDir + '/' + item;
            var fileData = [];
            var sql = 'insert into tbl_call_bill (domain_uuid,caller,called,ticketId,recordId,disconnect_cause,mtgSN,' +
              'authAction,setup_time,alert_time,connect_time,disconnect_time,session_time,report_time,recv_time) values'

            var readStream = fs.createReadStream(filePath, {
              flags: 'r',
              encoding: null,
              mode: 0666,
              autoClose: true
            });
            readStream.on('data', function(chunk) { // 当有数据流出时
              //判断并拼接buffer
              if(fileData.length == 0){
                fileData = new Buffer(chunk,'binary');
              }else{
                fileData = Buffer.concat([fileData,new Buffer(chunk,'binary')])
              }
            });

            readStream.on('end',function(){
              //读取完成后，解析信息
              var dataArr = new Buffer(fileData).toJSON().data;
              var startIndex = 0;
              var endIndex = 0;
              var parsedArr = [];
              for(var i=0;i< dataArr.length;i++){
                if(dataArr[i] == 124){
                  endIndex = i;
                  //裁剪数组
                  var cutArr = dataArr.slice(startIndex,endIndex);
                  parsedArr.push(cutArr)
                  startIndex = parseInt(i) + 1;
                }
              }
              //操作每一组buffer
              for(var j = 0;j< parsedArr.length;j++){
                try{
                  var domain_uuid,
                    buf = new Buffer(parsedArr[j]),
                    packet = radius.decode_without_secret({packet: buf,secret:'',no_secret:true}),
                    sessionId = packet.attributes['Acct-Session-Id'],
                    ticketId = packet.attributes['ticket-id'],
                    recordId = packet.attributes['record-id'] || 0,
                    caller = packet.attributes['Calling-Station-Id'],
                    called = packet.attributes['Called-Station-Id'],
                    disconnectCause = packet.attributes['disconnect-cause'],
                    mtgSN = packet.attributes['mtg-series-number'],
                    authAction = packet.attributes['ERRORCODE'],
                    sessionTime = packet.attributes['Acct-Session-Time'] || 0,
                    setupTime = packet.attributes['setup-time'],
                    alertTime = packet.attributes['alert-time'],
                    connectTime =  packet.attributes['connect-time'],
                    disconnectTime =  packet.attributes['disconnect-time'],
                    reportTime =  packet.attributes['report-time'] || '0000-00-00 00:00:00',
                    recvTime = global.localStaticToUTC(new Date().getTime());
                  if(setupTime != '0000-00-00 00:00:00'){
                    setupTime = global.localStaticToUTC(new Date(packet.attributes['setup-time']).getTime());
                  }
                  if(alertTime != '0000-00-00 00:00:00'){
                    alertTime = global.localStaticToUTC(new Date(packet.attributes['alert-time']).getTime());
                  }
                  if(connectTime != '0000-00-00 00:00:00'){
                    connectTime = global.localStaticToUTC(new Date(packet.attributes['connect-time']).getTime());
                  }
                  if(disconnectTime != '0000-00-00 00:00:00'){
                    disconnectTime = global.localStaticToUTC(new Date(packet.attributes['disconnect-time']).getTime());
                  }

                  //对MTG分组统计
                  if(!mtgStatistics[mtgSN]){
                    //初始化mtgStatistics
                    mtgStatistics[mtgSN] = {};
                    mtgStatistics[mtgSN]['dealStatus1'] = 0;
                    mtgStatistics[mtgSN]['dealStatus2'] = 0;
                    mtgStatistics[mtgSN]['dealStatus3'] = 0;
                    mtgStatistics[mtgSN]['dealStatus4'] = 0;
                    mtgStatistics[mtgSN]['dealStatus5'] = 0;
                    mtgStatistics[mtgSN]['dealStatus6'] = 0;
                    mtgStatistics[mtgSN]['totalProcess'] = 0;
                    mtgStatistics[mtgSN]['accessTotalProcess'] = 0;
                    mtgStatistics[mtgSN]['accessTotalTime'] = 0;
                    mtgStatistics[mtgSN]['disconnectCauseJSON'] = [];
                    //通话总数，通话总时长
                    if(authAction == 0 || authAction == 4){
                      mtgStatistics[mtgSN]['accessTotalProcess']++;
                      mtgStatistics[mtgSN]['accessTotalTime'] += sessionTime;
                      mtgStatistics[mtgSN]['dealStatus6']++;
                    }
                    //未接通原因统计
                    var isExistDisconnectCause = false;
                    for(var i=0;i<mtgStatistics[mtgSN]['disconnectCauseJSON'].length;i++){
                      if(mtgStatistics[mtgSN]['disconnectCauseJSON'][i].name = global[disconnectCause]){
                        isExistDisconnectCause = true;
                        mtgStatistics[mtgSN]['disconnectCauseJSON'][i].value++;
                        break;
                      }
                    }
                    //不存在时，初始化原因
                    if(!isExistDisconnectCause){
                      mtgStatistics[mtgSN]['disconnectCauseJSON'].push({name:global[disconnectCause],value: 1})
                    }
                    //状态数
                    if(authAction == 4){
                      mtgStatistics[mtgSN]['dealStatus4']++
                    }else if(authAction == 1){
                      mtgStatistics[mtgSN]['dealStatus1']++;
                    }else if(authAction == 2){
                      mtgStatistics[mtgSN]['dealStatus2']++;
                    }else if(authAction == 3){
                      mtgStatistics[mtgSN]['dealStatus3']++;
                    }else if(authAction == 5){
                      mtgStatistics[mtgSN]['dealStatus5']++;
                    }
                    mtgStatistics[mtgSN]['totalProcess']++;
                  }else{
                    //通话总数，通话总时长
                    if(authAction == 0 || authAction == 4){
                      mtgStatistics[mtgSN]['accessTotalProcess']++;
                      mtgStatistics[mtgSN]['accessTotalTime'] += sessionTime;
                      mtgStatistics[mtgSN]['dealStatus6']++;
                    }
                    //未接通原因统计
                    if(!mtgStatistics[mtgSN]['disconnectCauseJSON'][global[disconnectCause]]){
                      mtgStatistics[mtgSN]['disconnectCauseJSON'][global[disconnectCause]] = 0;
                    }else{
                      mtgStatistics[mtgSN]['disconnectCauseJSON'][global[disconnectCause]] ++;
                    }
                    //状态数
                    if(authAction == 4){
                      mtgStatistics[mtgSN]['dealStatus4']++
                    }else if(authAction == 1){
                      mtgStatistics[mtgSN]['dealStatus1']++;
                    }else if(authAction == 2){
                      mtgStatistics[mtgSN]['dealStatus2']++;
                    }else if(authAction == 3){
                      mtgStatistics[mtgSN]['dealStatus3']++;
                    }else if(authAction == 5){
                      mtgStatistics[mtgSN]['dealStatus5']++;
                    }
                    mtgStatistics[mtgSN]['totalProcess']++;
                  }

                  //判断是否连接cloud,单点部署域为0
                  if(config.connect_to_cloud == "true"){
                    domain_uuid = domain[packet.attributes['NAS-Identifier']]; //域id
                  }else{
                    domain_uuid = '0';
                  }

                  if(domain_uuid == undefined){
                    console.log("Call Bill can not find domain_uuid for name:" + packet.attributes['NAS-Identifier']);
                    cb('domain_uuid not found!');
                    break;
                  }else{
                    //组合sql语句
                    if(parsedArr.length == 1){
                      sql += "(" + domain_uuid + ',' + caller + "," + called + "," + ticketId + "," + recordId + "," +  disconnectCause + ",'" + mtgSN + "'," + authAction + ",'" +  setupTime + "','" +
                      alertTime + "','" +  connectTime + "','" +  disconnectTime + "'," +  sessionTime + ",'" +  reportTime + "','" +
                      recvTime + "')";
                    }else{
                      if(j != parsedArr.length-1){
                        sql += "(" +  domain_uuid + ',' +  caller + "," +  called + "," + ticketId + "," + recordId + "," +  disconnectCause + ",'" + mtgSN + "'," + authAction + ",'" +  setupTime + "','" +
                        alertTime + "','" +  connectTime + "','" +  disconnectTime + "'," +  sessionTime + ",'" +  reportTime + "','" +
                        recvTime + "'),";
                      }else{
                        sql += "(" +  domain_uuid + ',' +  caller + "," +  called + "," + ticketId + "," + recordId + ","  +  disconnectCause + ",'" + mtgSN + "'," + authAction + ",'" +  setupTime + "','" +
                        alertTime + "','" +  connectTime + "','" +  disconnectTime + "'," +  sessionTime + ",'" +  reportTime + "','" +
                        recvTime + "')";
                      }
                    }
                  }
                }catch(e){
                  logger.error(e);
                  cb(err);
                }
              }
              //话单写入请求数加1
              getRequestNum++;
              //插入数据库
              //console.log(sql)
              var writeStartTime = process.hrtime();
              insertSql(sql,function(err){
                //处理入库用时
                var writeDealTime = process.hrtime(writeStartTime);
                writeDealTime = (writeDealTime[0] * 1e9 + writeDealTime[1])/1000000;
                lastingTotalDelay += writeDealTime;
                if(err){
                  cb(err)
                }else{
                  //写入成功数加1
                  writeSuccessNum++;
                  cb(null);
                }
              });
            });
          },function(err){
            if(err){
              //入库发生错误时，产生错误告警
              var alarm_object,alarm_cause;
              if(err == 'domain_uuid not found!'){
                alarm_object = 2;
                alarm_cause = 'CALL_TICKET_ALARM'
              }else{
                alarm_object = 1;
                alarm_cause = 'DATABASE_ALARM'
              }
              var sql = 'insert into tbl_alarm (level,alarm_object,alarm_cause,alarm_detail,create_time) values(4,' + alarm_object +
                  ',"' + alarm_cause + '",\'' + JSON.stringify(err) + '\',"' + global.localStaticToUTC(new Date().getTime()) + '")';
              mysql1.process(sql);
            }else{
              //入库完成，清空该目录
              files.forEach(function(fileName){
                fs.unlinkSync(callBillDir + '/' + fileName);
              })
            }
          })
        }
      })
    },10 * 1000)
  }catch(e){
    logger.error(e);
    //话单服务未知错误时，产生错误告警
    var sql = 'insert into tbl_alarm (level,alarm_object,alarm_cause,alarm_detail,create_time) values(4,2,"CALL_TICKET_ALARM"' +
     ',\'' + JSON.stringify(err) + '\',"' + global.localStaticToUTC(new Date().getTime()) + '")';
    mysql1.process(sql);
  }
};

function insertSql(sql,cb){
  mysql1.Cb_process(sql,null,function(err){
    if(err){
      logger.error('话单存入数据库错误！');
      logger.error(err);
      cb(err);
    }else{
      cb(null);
    }
  })
}


