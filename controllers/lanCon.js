/**
 * Created by Rainc on 2015/7/6.
 */
var lanCn = require('../lan/lan_cn.json');
var lanEn = require('../lan/lan_en.json');

exports.getLanValue = function(value){
  if(localLan == 'zh_CN'){
    return lanCn[value];
  }else if(localLan == 'en_US'){
    return lanEn[value];
  }
}