/**
 * Created by Rainc on 2015/9/7.
 */

var fs = require('fs');
var crypto = require('crypto');
var execSync = require('child_process').execSync;
var mysql1 = require('./basedao.js');
var lanCon = require('./lanCon.js');
var http = require('http');
var querystring =  require('querystring');

var log4js = require('log4js');
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

var callBillDir = './callBillDir';

agentTotalDelay = 0;   //写入临时存储区总用时
agentNums = 0 ;        //写入临时存储区次数

exports.callBillApi = function(req,res){
  var action = req.query.action || req.body.action;
  if(!action || action == ''){
    res.json({success:false,result:{},errMsg:"require param action!"})
  }else{
    //判断是否登录，未登录直接跳转到登录界面
    if(!req.session.isVisit && action != 'loginAction'){
      return  res.json({success:false,result:{},errMsg:"Session timeout!Please relogin!"})
    }
    try{
      switch(action){
        case 'loginAction':
          var dcUserName = req.body.dcUserName || req.query.dcUserName;
          var dcPassword = req.body.dcPassword || req.query.dcPassword;
          localLan = req.body.localLan || req.query.localLan;        //设置全局语言
          if(dcUserName != config.account ){
            res.json({success:false,result:{},errMsg:lanCon.getLanValue('login_failed_userName')})
          }else if(crypto.createHash('md5').update(dcPassword).digest('hex').toUpperCase() != config.password){
            res.json({success:false,result:{},errMsg:lanCon.getLanValue('login_failed_password')})
          }else{
            req.session.isVisit = 1;
            res.json({success:true,result:{},errMsg:""});
          }
          break;
        case 'logoutAction':
          req.session.isVisit = null;
          res.json({success:true,result:{},errMsg:""});
          break;
        case 'changePwd':
          try{
            var password = config.password;
            var account = config.account;
            var oldPwd = req.query.oldPwd || req.body.oldPwd;
            var newPwd = req.query.newPwd || req.body.newPwd;
            if(!oldPwd || !newPwd ){
              res.json({success:false,result:{},errMsg:lanCon.getLanValue('Missing Parameters!')})
            }else if(crypto.createHash('md5').update(oldPwd).digest('hex').toUpperCase() != password){
              res.json({success:false,result:{},errMsg:lanCon.getLanValue('Old password incorrect!')})
            }else{
              config.password = crypto.createHash('md5').update(newPwd).digest('hex').toUpperCase();
              //同时修改配置文件
              var cmd = 'sed -i "s/.*\\"password\\":.*/  \\"password\\": \\"' + password + '\\",/" ' + config.serverPath + '/etc/config.json';
              execSync(cmd);
              res.json({success:true,result:{},errMsg:''});
            }
          }catch(e){
            logger.error('exec error: ' + e);
            res.json({success:false,result:{},errMsg:'Exec error!'})
          }
          break;
        case 'getServerConfiguration':
          try{
            var values ={
              server_ip:config.server_ip,
              web_port:config.web_port,
              db_host:config.db_host,
              db_user:config.db_user,
              db_passwd:config.db_passwd,
              db_database:config.db_database,
              cpuAlarmRate:config.cpuAlarmRate,
              serverMemoryAlarmRate:config.serverMemoryAlarmRate,
              appMemoryAlarmRate:config.appMemoryAlarmRate,
              diskUsedAlarmRate:config.diskUsedAlarmRate
            }
            res.json({success:true,result:values,errMsg:""})
          }catch(e){
            res.json({success:false,result:{},errMsg:""})
          }
          break;
        case 'saveServerConfiguration':
          try{
            var obj ={};
            obj.server_ip = req.body.server_ip;
              obj.web_port = req.body.web_port;
              obj.db_host = req.body.db_host;
              obj.db_user = req.body.db_user;
              obj.db_passwd = req.body.db_passwd;
              obj.db_database = req.body.db_database;
              obj.cpuAlarmRate = req.body.cpuAlarmRate;
              obj.serverMemoryAlarmRate = req.body.serverMemoryAlarmRate;
              obj.appMemoryAlarmRate = req.body.appMemoryAlarmRate;
              obj.diskUsedAlarmRate = req.body.diskUsedAlarmRate;
            //循环修改config设置
            for(var i in obj){
              //更新主进程config值
              config[i] = obj[i];
              var cmd = '';
              if(i != 'diskUsedAlarmRate'){
                cmd += 'sed -i "s/.*\\"' + i +  '\\":.*/  \\"' + i + '\\": \\"' + obj[i] + '\\",/" ' + config.serverPath + '/config.json';
              }else{
                cmd += 'sed -i "s/.*\\"' + i +  '\\":.*/  \\"' + i + '\\": \\"' + obj[i] + '\\"/" ' + config.serverPath + '/config.json';
              }
              execSync(cmd);
            }
            res.json({success:true,result:{},errMsg:""})
          }catch(e){
            logger.error('exec error: ' + e);
            res.json({success:false,result:{},errMsg:"Exec error!"})
          }
          break;
        case 'restartNow':
          try{
            var cmd = 'pm2 restart ' + config.serverPath + '/bin/www';
            execSync(cmd);
            res.json({success:true,result:{},errMsg:''})
          }catch(e){
            logger.error('exec error: ' + e);
            res.json({success:false,result:{},errMsg:"Exec error!"})
          }
          break;
        case 'getAlarmLog':
          var sql = 'select count(*) from tbl_alarm';
          mysql1.find(sql,function(err,data){
            if(err){
              logger.error(err);
              res.json({success:false,result:{},errMsg:"Database error!"})
            }else{
              var total  = data[0]['count(*)'];
              var search = req.body.search;
              var limit = req.body.limit;
              var order = req.body.order;
              var offset = req.body.offset;
              var sort = req.body.sort || 'create_time';
              var sql = 'select * from tbl_alarm ';
              if(search){
                if(search == lanCon.getLanValue('debug')){
                  sql += ' where level=1'
                }else if(search == lanCon.getLanValue('notice')){
                  sql += ' where level=2'
                }else if(search == lanCon.getLanValue('warning')){
                  sql += ' where level=3'
                }else if(search == lanCon.getLanValue('error')){
                  sql += ' where level=4'
                }else if(search == lanCon.getLanValue('serious')){
                  sql += ' where level=5'
                }else if(search == lanCon.getLanValue('unknown')){
                  sql += ' where alarm_object=0'
                }else if(search == lanCon.getLanValue('database')){
                  sql += ' where alarm_object=1'
                }else if(search == lanCon.getLanValue('call ticket')){
                  sql += ' where alarm_object=2'
                }else if(search == lanCon.getLanValue('cloud')){
                  sql += ' where alarm_object=3'
                }else if(search == lanCon.getLanValue('cpu userate')){
                  sql += ' where alarm_object=4'
                }else if(search == lanCon.getLanValue('server memory userate')){
                  sql += ' where alarm_object=5'
                }else if(search == lanCon.getLanValue('app memory userate')){
                  sql += ' where alarm_object=6'
                }else if(search == lanCon.getLanValue('disk userate')){
                  sql += ' where alarm_object=7'
                }else if(search == lanCon.getLanValue('confirmed')){
                  sql += ' where confirmed=2'
                }else if(search == lanCon.getLanValue('unconfirmed')){
                  sql += ' where confirmed=1'
                }
              }
              sql += ' order by ' + sort + ' ' + order + ' limit ' + limit + ' offset '+ offset;
              mysql1.find(sql,function(err,data1){
                if(err){
                  logger.error(err);
                  res.json({success:false,result:{},errMsg:err});
                }else{
                  res.json({success:true,result:{list:data1,total:total},errMsg:""});
                }
              });
            }
          })
          break;
        case 'confirmAlarms':
          var alarms = req.body.alarms;
          var sql =  'update tbl_alarm set confirmed=2,confirm_time="' + localStaticToUTC(new Date().getTime()) +  '" where uuid in (';
          if(typeof(alarms) == 'string'){
            sql += alarms + ')';
          }else{
            for(var i =0;i< alarms.length -1;i++){
              sql += alarms[i] + ','
            }
            sql += alarms[alarms.length -1] + ")";
          }
          mysql1.Cb_process(sql,null,function(err){
            if(err){
              logger.error(err);
              res.json({success:false,result:{},errMsg:"Database error!"})
            }else{
              res.json({success:true,result:{},errMsg:""})
            }
          });
          break;
        case 'deleteAlarms':
          var alarms = req.body.alarms;
          var sql =  'delete from tbl_alarm where uuid in (';
          if(typeof(alarms) == 'string'){
            sql += alarms + ')';
          }else{
            for(var i =0;i< alarms.length -1;i++){
              sql += alarms[i] + ','
            }
            sql += alarms[alarms.length -1] + ")";
          }
          mysql1.Cb_process(sql,null,function(err){
            if(err){
              logger.error(err);
              res.json({success:false,result:{},errMsg:'Database error!'})
            }else{
              res.json({success:true,result:{},errMsg:""})
            }
          });
          break;
        case 'emptyAlarms':
          var sql =  'truncate tbl_alarm';
          mysql1.Cb_process(sql,null,function(err){
            if(err){
              logger.error(err);
              res.json({success:false,result:{},errMsg:'Database error!'})
            }else{
              res.json({success:true,result:{},errMsg:""})
            }
          });
          break;
        case 'writeCallInfo':
          try{
            var fileName = req.body.fileName,
              fileData = req.body.fileData,
              filePath = callBillDir + '/' + fileName;
            //写入文件
            var writeStartTime = process.hrtime();
            fs.appendFileSync(filePath,fileData,{encoding :'binary'});
            var writeDealTime = process.hrtime(writeStartTime);
            writeDealTime = (writeDealTime[0] * 1e9 + writeDealTime[1])/1000000;
            agentTotalDelay += writeDealTime;
            agentNums ++;
          }catch(e){
            //话单服务写入临时存储文件发生错误时，产生错误告警
            var sql = 'insert into tbl_alarm (level,alarm_object,alarm_cause,alarm_detail,create_time) values(4,2,"CALL_TICKET_ALARM"' +
              ',\'' + JSON.stringify(e) + '\',"' + localStaticToUTC(new Date().getTime()) + '")';
            mysql1.process(sql);
          }
          break;
        case 'getCallInfo':
          var sql = "select count(*) from tbl_call_bill ";
          var search = req.body.search;
          var order = req.body.order;
          var sort = req.body.sort || 'recv_time';
          var limit = req.body.limit;
          var offset = req.body.offset;
          var labelValue = req.body.labelValue;
          var selectValue = req.body.selectValue;
          var textValue = req.body.textValue;

          if(labelValue&&selectValue&& textValue){
            if(labelValue==1){
              labelValue = 'disconnect_cause'
            }else if(labelValue==2){
              labelValue = 'caller'
            }else if(labelValue==3){
              labelValue = 'called'
            }else if(labelValue==4){
              labelValue = 'mtgSN'
            }else if(labelValue==5){
              labelValue = 'authAction'
              if(textValue == '禁呼'){
                textValue = 1
              }else if(textValue == '禁透'){
                textValue = 2
              }else if(textValue == '禁呼禁透'){
                textValue = 3
              }else if(textValue == '录音'){
                textValue = 4
              }else if(textValue == '频率控制' || textValue == '频率'){
                textValue = 5
              }
            }
            if(selectValue == 1){
              sql += ' where rec_status=0 and ' + labelValue + ' = ' + textValue;
            }else{
              sql += ' where rec_status=0 and ' + labelValue + ' != ' + textValue;
            }
          }

          //获取条目数
          mysql1.find(sql,function(err,data){
            if(err){
              logger.error(err);
              res.json({success:false,result:{},errMsg:"Database error!"});
            }else{
              var total = data[0]['count(*)'];
              sql = "select * from tbl_call_bill ";
              if(labelValue&&selectValue&& textValue){
                if(labelValue==1){
                  labelValue = 'disconnect_cause'
                }else if(labelValue==2){
                  labelValue = 'caller'
                }else if(labelValue==3){
                  labelValue = 'called'
                }else if(labelValue==4){
                  labelValue = 'mtgSN'
                }else if(labelValue==5){
                  labelValue = 'authAction'
                  if(textValue == '禁呼'){
                    textValue = 1
                  }else if(textValue == '禁透'){
                    textValue = 2
                  }else if(textValue == '禁呼禁透'){
                    textValue = 3
                  }else if(textValue == '录音'){
                    textValue = 4
                  }else if(textValue == '频率控制' || textValue == '频率'){
                    textValue = 5
                  }
                }
                if(selectValue == 1){
                  sql += ' where rec_status=0 and ' + labelValue + ' = ' + textValue;
                }else{
                  sql += ' where rec_status=0 and ' + labelValue + ' != ' + textValue;
                }
              }
              sql += ' order by ' + sort + ' ' + order + ' limit ' + limit + ' offset ' + offset;
              mysql1.find(sql,function(err,data1){
                if(err){
                  logger.error(err);
                  res.json({success:false,result:{},errMsg:"Database error!"});
                }else{
                  res.json({success:true,result:{list:data1,total:total},errMsg:""});
                }
              })
            }
          })
          break;
        case 'deleteCallItems':
          var items = req.body.items;
          var sql =  'delete from tbl_call_bill where uuid in (';
          if(typeof(items) == 'string'){
            sql += items + ")";
          }else{
            for(var i =0;i< items.length -1;i++){
              sql += items[i] + ','
            }
            sql += items[items.length -1] + ")";
          }
          mysql1.Cb_process(sql,null,function(err){
            if(err){
              logger.error(err);
              res.json({success:false,result:{},errMsg:"Database error!"});
            }else{
              res.json({success:true,result:{},errMsg:""});
            }
          })
          break;
        case 'emptyCallItems':
          var sql = 'truncate tbl_call_bill';
          mysql1.Cb_process(sql,null,function(err){
            if(err){
              logger.error(err);
              res.json({success:false,result:{},errMsg:"Database error!"});
            }else{
              res.json({success:true,result:{},errMsg:""});
            }
          })
          break;
        case 'exportCallTicket':
          var downType = req.body.downType;
          var items = req.body.items;
          var sql = 'select * from tbl_call_bill where uuid in (';
          if(typeof(items) == 'string'){
            sql += items + ")";
          }else{
            for(var i =0;i< items.length -1;i++){
              sql += items[i] + ','
            }
            sql += items[items.length -1] + ")";
          }
          mysql1.find(sql,function(err,data){
            if(err){
              logger.error(err);
              res.json({success:false,result:{},errMsg:"Database error!"})
            }else{
              var filePath = "";
              var lastIndex = "";
              if(downType == 'csv'){
                filePath = 'public/exportDir/' + new Date().getTime() + parseInt(Math.random()*1000) + '.csv';
                lastIndex = '\t\n';
              }else{
                filePath = 'public/exportDir/' + new Date().getTime() + parseInt(Math.random()*1000) + '.txt';
                lastIndex = '\r\n';
              }
              var value = "domain_uuid,caller,called,session_time,mtgSN,authAction,disconnect_cause,report_time," +
                "recv_time,setup_time,alert_time,connect_time,disconnect_time" + lastIndex;
              for(var i=0;i<data.length;i++){
                value += data[i]['domain_uuid'] + ',' + data[i]['caller'] + ',' + data[i]['called'] + ',' + data[i]['session_time']
                + ',' + data[i]['mtgSN']+ ',' + data[i]['authAction']+  ',' +  data[i]['disconnect_cause'] +  ','
                + (data[i]['report_time'] == '0000-00-00 00:00:00' ? '0000-00-00 00:00:00': localStaticToUTC(data[i]['report_time'])) + ','
                + (data[i]['recv_time'] == '0000-00-00 00:00:00' ? '0000-00-00 00:00:00': localStaticToUTC(data[i]['recv_time'])) + ','
                + (data[i]['setup_time'] == '0000-00-00 00:00:00' ? '0000-00-00 00:00:00': localStaticToUTC(data[i]['setup_time'])) + ','
                + (data[i]['alert_time'] == '0000-00-00 00:00:00' ? '0000-00-00 00:00:00': localStaticToUTC(data[i]['alert_time'])) + ','
                + (data[i]['connect_time'] == '0000-00-00 00:00:00' ? '0000-00-00 00:00:00': localStaticToUTC(data[i]['connect_time'])) + ','
                + (data[i]['disconnect_time'] == '0000-00-00 00:00:00' ? '0000-00-00 00:00:00': localStaticToUTC(data[i]['disconnect_time']))
                + lastIndex
              }

              fs.open(filePath,'w',function(err,fid){
                if(err){
                  res.json({success:false,result:{},errMsg:"File operation error!"})
                }else{
                  fs.writeFile(filePath, value,{encoding :'utf8'}, function (err) {
                    if (err) {
                      res.json({success:false,result:{},errMsg:"File operation error!"})
                    }else{
                      res.json({success:true,result:{href: filePath.substr(7),count:data.length},errMsg:""})
                    }
                  });
                }
              })
            }
          });
          break;
        case 'getPerformanceStatistic':
          var sql = "select server_cpu_userate,server_memory_userate,server_disk_userate,app_memory_userate,loadavg_1m," +
            "loadavg_5m,loadavg_15m,get_request_num,write_success_num,agent_avg_delay,lasting_avg_delay,create_time  from tbl_sys_1";
          mysql1.find(sql,function(err,data){
            if(err){
              logger.error(err);
              res.json({success:false,result:{},errMsg:"Database error!"});
            }else{
              res.json({success:true,result:data,errMsg:""});
            }
          })
          break;
        case 'getPerformanceCurrentStatistic':
          try{
            var values = [{
              server_cpu_userate:CPU_UTILIZATION,
              server_memory_userate:SERVER_MEMORY_UTILIZATION,
              server_disk_userate:DISK_UTILIZATION,
              app_memory_userate:PROCESS_MEMORY_UTILIZATION,
              create_time: localStaticToUTC(new Date().getTime()),
              loadavg1: LOADAVG_1M,
              loadavg5: LOADAVG_5M,
              loadavg15: LOADAVG_15M,
              writeSuccessNum:writeSuccessNum,
              getRequestNum:getRequestNum,
              lastingAvgDelay:Math.round(lastingTotalDelay / getRequestNum) || 0,
              agentAvgDelay:Math.round(agentTotalDelay / agentNums) || 0
            }];
            res.json({success:true,result:values,errMsg:""});
          }catch(e){
            logger.error(e);
            res.json({success:false,result:{},errMsg:"Server operation error!"});
          }
          break;
        case 'getDevStatistic':
          var sql = "select count(*) from tbl_dev_1 ";
          var search = req.body.search;
          var sort = req.body.sort || 'create_time';
          var order = req.body.order;
          var limit = req.body.limit;
          var offset = req.body.offset;

          if(search){
            sql += "where create_time like '%" + search + "%' or dev_sn like '%" + search + "%' "
          }

          //获取条目数
          mysql1.find(sql,function(err,data){
            if(err){
              logger.error(err);
              res.json({success:false,result:{},errMsg:"Database error!"});
            }else{
              var total = data[0]['count(*)'];
              sql = "select * from tbl_dev_1 ";
              if(search){
                sql += "where create_time like '%" + search + "%' or dev_sn like '%" + search + "%' "
              }
              sql += ' order by ' + sort + ' ' + order + ' limit ' + limit + ' offset ' + offset;
              mysql1.find(sql,function(err,data1){
                if(err){
                  logger.error(err);
                  res.json({success:false,result:{},errMsg:"Database error!"});
                }else{
                  res.json({success:true,result:{list:data1,total:total},errMsg:""});
                }
              })
            }
          })
          break;
        case  'getValidRecord':
          var recordId = req.body.recordId;
          var caller = req.body.caller;
          var called = req.body.called;
          var postData = querystring.stringify({
            'action': 'getValidRecord',
            'recordId' : recordId,
            'caller' : caller,
            'called' : called
          });
          var request =http.request({
            host: config.recordServerHost,
            port: config.recordServerPort,
            method: 'POST',
            path: '/api',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Content-Length': postData.length
            }
          },function(response){
            var resData = ''
            response.setEncoding('utf8');
            response.on('data', function (chunk) {
              resData += chunk;
            });
            response.on('end',function(){
              var success = JSON.parse(resData).success;
              var result = JSON.parse(resData).result;
              var errMsg = JSON.parse(resData).errMsg;
              res.json({success:success,result:{list:result.list,ip:config.recordServerHost,port:config.recordServerPort},errMsg:errMsg});
            })
          })
          request.on('error', function(e) {
            logger.error(e);
            res.json({success:false,result:{},errMsg:"Network error!"});
          });
          request.write(postData);
          request.end();
          break;
        default:
          res.json({success:false,result:{},errMsg:'Not valid action!'})
      }
    }catch(err){
      logger.error(err);
      res.json({success:false,result:{},errMsg:"Unkown error!"})
    }
  }
}

Date.prototype.format = function(format){
  var o = {
    "M+" : this.getMonth()+1, //month
    "d+" : this.getDate(), //day
    "h+" : this.getHours(), //hour
    "m+" : this.getMinutes(), //minute
    "s+" : this.getSeconds(), //second
    "q+" : Math.floor((this.getMonth()+3)/3), //quarter
    "S" : this.getMilliseconds() //millisecond
  }

  if(/(y+)/.test(format)) {
    format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  }

  for(var k in o) {
    if(new RegExp("("+ k +")").test(format)) {
      format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
    }
  }
  return format;
};
function localStaticToUTC(value){
  var da = new Date(value);
  var off = da.getTimezoneOffset();
  da.setHours(da.getHours()+off/60);
  var va = da.format("yyyy-MM-dd hh:mm:ss");
  return va;
}
