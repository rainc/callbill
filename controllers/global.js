/**
 * Created by Rainc on 2015/9/8.
 */
exports.global = {
  CC_CAUSE_E : [
    'CCS_NONE'                    ,            /* 无原因值 */
    'CCS_NORM_CLEAR'             ,            /* 正常释放 */
    'CCS_NUM_ERR'                 ,            /* 未分配的号码 */
    'CCS_NUM_WAITING'             ,            /* 被叫号码不完全 */
    'CCS_USER_FAULT'              ,            /* 终点故障 */
    'CCS_USER_BUSY'              ,            /* 用户忙 */
    'CCS_PEER_NO_RESP'            ,            /* 对端无响应 */
    'CCS_USER_NO_ANSWER'          ,            /* 被叫无应答 */
    'CCS_NO_SERV_RIGHTS'          ,            /* 无业务权限 */
    'CCS_USR_AUTH_FAIL'           ,            /* 用户认证失败 */
    'CCS_BILL_AUTH_FAIL'          ,           /* 计费认证失败 */
    'CCS_CARD_NO_EXIST'           ,           /* 卡不存在 */
    'CCS_CARD_EXPIRE'             ,           /* 卡失效 */
    'CCS_CARD_LOCKED'             ,           /* 卡锁定 */
    'CCS_LACK_FEE'                ,           /* 卡余额不足 */
    'CCS_NO_BIND'                 ,           /* 话机未绑定 */
    'CCS_PSWD_ERR'                ,           /* 密码错误 */
    'CCS_CALL_FORBID'             ,           /* 呼叫被禁止 */
    'CCS_NO_ROUTE'                ,           /* 无路由到达规定的转接网络 */
    'CCS_NETWORK_BUSY'            ,           /* 网络忙 */
    'CCS_NETWORK_ERR'             ,           /* 网络故障 */
    'CCS_CONFIG_ERR'              ,           /* 数据配置错误 */
    'CCS_CRCX_FAIL'               ,           /* CRCX失败 */
    'CCS_MDCX_FAIL'               ,           /* MDCX失败 */
    'CCS_DLCX_IND'                ,           /* 终端主动删除 */
    'CCS_TM_CRCX_FAIL'            ,           /* TM创建连接失败 */
    'CCS_TM_MDCX_FAIL'            ,           /* TM修改连接失败 */
    'CCS_ENCODE_FAIL'             ,           /* 消息编码失败 */
    'CCS_DECODE_FAIL'             ,           /* 消息解码失败 */
    'CCS_NEGOTIATE_FAIL'          ,           /* 能力参数协商失败 */
    'CCS_MULTI_REDIRECT'          ,           /* 多次重定向 */
    'CCS_REDIRECT_NUM_ERR'        ,           /* 重定向号码错误 */
    'CCS_DIAL_TIMEOUT'            ,           /* 拨号超时 */
    'CCS_SETUP_TIMEOUT'           ,           /* 呼叫建立超时 */
    'CCS_CONNECT_TIMEOUT'         ,           /* 连接建立超时 */
    'CCS_SYSTEM_UPDATE'           ,           /* 系统正在升级 */
    'CCS_SVR_NO_RESP'             ,           /* 服务器无响应 */
    'CCS_RADIUS_SVR_LOST'         ,           /* RADIUS服务器失去联系 */
    'CCS_SVR_PROC_ERROR'          ,           /* 服务器处理错误 */
    'CCS_USER_REJECT'             ,           /* 用户拒绝 */
    'CCS_ERR_USER_INPUT'          ,           /* 错误的用户输入 */
    'CCS_CCB_CHECK_ERR'           ,           /* CCB状态检查错误 */
    'CCS_DIAL_NEED_POUND'         ,           /* 拨号需要按#号键结束 */
    'CCS_IAD_LOST'                ,           /* IAD断链 */
    'CCS_TRUNK_LOST'              ,           /* 中继中断 */
    'CCS_SYS_EXCEPTION'           ,           /* 处理异常 */
    'CCS_UNKNOWN_ERROR'           ,           /* 未知错误 */
    'CCS_RTX_CALLING_ERR'         ,           /* RTX主叫未配置*/
    'CCS_RTX_CALLING_BUSY'        ,           /* RTX主叫忙 */
    'CCS_RTX_CALLING_NO_RSP'      ,           /* RTX主叫未摘机*/
    'CCS_SESSION_TIMEOUT'         ,           /* 通话时长超限 */
    'CCS_SIP_USER_LOST'           ,           /* SIP用户掉线 */
    'CCS_LE_CONGESTION'           ,           /* 本地交换机拥塞 */
    'CCS_SERV_NOT_MATCH'          ,           /* 终端业务不匹配 */
    'CCS_NO_LOCAL_CIRCUIT'        ,           /* 无本地电路资源 */
    'CCS_PORT_DISABLED'           ,           /* 端口已禁用 */
    'CCS_CF_AUTH_FAIL'            ,           /* 呼叫转移计费认证失败 */
    'CCS_AREA_RESTRICT'           ,           /* 区域限制,部分业务不让进行 */
    'CCS_CALL_CHECK_ERR'          ,           /* 呼叫检查超时 */
    'CCS_DSP_FAIL'                ,           /* DSP故障 */
    'CCS_PSTN_FAIL'               ,           /* PRA或SS7接口故障 */
    'CCS_SIP_CONNECT_TIMEOUT'     ,           /* SIP200或200响应消息丢失导致拆呼 */
    'CCS_GET_DB_DATA_FAIL'       ,           /* 获取数据库数据失败 */
    'CCS_TEMPORARY_FAILURE'		    ,		      	/*临时错误*/
    'CCS_SRV_OPT_UNAVA'			      ,		      	/*服务不允许*/
    'CCS_SRV_OPT_IMPLEMENTED'		  ,		      	/*服务不可实施*/
    'CCS_NUM_CHANGE'				      ,	      		/*号码更改*/
    'CCS_BAD_GATEWAY'				      ,	      		/*网关不可用*/
    'CCS_NOT_IMPLEMENTED'			    ,		      	/*不可实施*/
    'CCS_NOT_ACCEPTABLE_HERE'     ,		      	/*不接受*/
    'CCS_127_INTERWORKING'     	  		      	/*互通问题*/
  ],
  localStaticToUTC: function (value){
    var da = new Date(value);
    var off = da.getTimezoneOffset();
    da.setHours(da.getHours()+off/60);
    var va = da.format("yyyy-MM-dd hh:mm:ss");
    return va;
  }
}