/**
 * Created with JetBrains WebStorm.
 * User: shihengwen
 * Date: 2013/10/18
 * Time: 下午 2:45
 * To change this template use File | Settings | File Templates.
 */
var mysql = require('mysql');

var db_config = {
  host: config.db_host,
  user: config.db_user,
  password: config.db_passwd,
  database: config.db_database,
  port: 3306
};

// 创建连接池
var pool = mysql.createPool(db_config);
var connection;


function getPoolConn(){
  return pool;
}

function getConn(){
  handleDisconnect(function(err,connection){
    if(err){
      return undefined;
    }else{
      return connection;
    }
  });
}
function handleDisconnect(cb) {
  connection = mysql.createConnection(db_config); // 创建连接对象

  // 如果断线自动重连
  connection.on('error', function (err) {
    console.log('db error', err);
    config.connected_to_database = false;
    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
      getConn();
    } else {
    }
  });

  // 连接失败
  connection.connect(function (err) {
    if (err) {
      config.connected_to_database = false;
      console.log('error when connecting to db:', err);
      //setTimeout(handleDisconnect, 2000);
      cb(err)
    } else {
      config.connected_to_database = true;
      cb(null, connection);
    }
  });
}

exports.getPoolConn = getPoolConn;
exports.getConn = getConn;
exports.handleDisconnect = handleDisconnect;
