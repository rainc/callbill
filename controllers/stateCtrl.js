/**
 * Created by Rainc on 2015/9/15.
 */
var global = require('./global.js').global;
var mysql1 = require('./basedao.js');
var systemStatus = require(config.systemStatusServerPath);

var log4js = require('log4js');
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

ticket_sys_serial_num = 1;   //按服务器流水号
ticket_dev_serial_num = 1;   //按设备流水号

exports.insertIntoTable = function(){
  //初始化时,更新系统状态信息，等待3秒后，插入一笔数据
  setTimeout(function(){
    systemStatus.init(function(err){
      if(err){
        logger.error(err)
        //当操作出错时，产生错误告警
        var sql = 'insert into tbl_alarm (level,alarm_object,alarm_cause,alarm_detail,create_time) values(4,2,"FAIL_TO_GET_SERVER_PERFORMANCE"' +
          ',\'' + JSON.stringify(err) + '\',"' + global.localStaticToUTC(new Date().getTime()) + '")';
        mysql1.process(sql);
      }else{
        sys_insert_1();
        dev_insert_1();
        alarm_check();
      }
    });
  },3000);

  //每隔1分钟，判断tbl_sys_1或tbl_authnum_dev_1数据量是否大于设置值，大于或等于的话重头开始进行更新，否则插入一笔数据
  setInterval(function(){
    systemStatus.init(function(err){
      if(err){
        logger.error(err);
        //当操作出错时，产生错误告警
        var sql = 'insert into tbl_alarm (level,alarm_object,alarm_cause,alarm_detail,create_time) values(4,2,"FAIL_TO_GET_SERVER_PERFORMANCE"' +
          ',\'' + JSON.stringify(err) + '\',"' + global.localStaticToUTC(new Date().getTime()) + '")';
        mysql1.process(sql);
      }else{
        sys_insert_1();
        dev_insert_1();
        //同时检查是否超过频率，超过则进行告警
        alarm_check();
      }
    });
  },60 * 1000)
}

function sys_insert_1(){
  var agentAvgDelay = Math.round(agentTotalDelay / agentNums) || 0;
  var lastingAvgDelay = Math.round(lastingTotalDelay / getRequestNum) || 0;
  //检测该服务器数据是否大于设置值
  var sql = "select count(*) as total from tbl_sys_1 where sys_uuid=" + config.server_id;
  mysql1.find(sql,function(err,rows){
    if(err){
      logger.error("查询tbl_sys_1出错！");
      logger.error(err);
    }else{
      if(rows[0].total < config.ticket_count_limit){
        sql = "insert into tbl_sys_1 (rec_status,sys_uuid,serial_no,create_time," +
        "server_cpu_userate,server_memory_userate,server_disk_userate,app_memory_userate,loadavg_1m,loadavg_5m,loadavg_15m,get_request_num" +
        ",write_success_num,agent_avg_delay,lasting_avg_delay) values(" +
        0 + "," + config.server_id + "," + ticket_sys_serial_num + ",'" + global.localStaticToUTC(new Date().getTime()) +
        "','" + CPU_UTILIZATION + "','" + SERVER_MEMORY_UTILIZATION + "','" + DISK_UTILIZATION
        + "','" + PROCESS_MEMORY_UTILIZATION + "','" + LOADAVG_1M + "','" + LOADAVG_5M
        + "','" + LOADAVG_15M + "','" + getRequestNum + "','" + writeSuccessNum
        + "','" + agentAvgDelay + "','" + lastingAvgDelay  + "')";
        console.log(sql);
        mysql1.process(sql);
        ticket_sys_serial_num ++;
      }else{
        if(ticket_sys_serial_num - 1 == config.ticket_count_limit){
          ticket_sys_serial_num = 1;
        }
        sql = "update tbl_sys_1 set create_time='" + global.localStaticToUTC(new Date().getTime())  + "',server_cpu_userate='"
        + CPU_UTILIZATION + "',server_memory_userate='" + SERVER_MEMORY_UTILIZATION + "',server_disk_userate='"
        + DISK_UTILIZATION + "',app_memory_userate='" + PROCESS_MEMORY_UTILIZATION  + "',agent_avg_delay='" + agentAvgDelay
        + "',lasting_avg_delay='" + lastingAvgDelay  + "',loadavg_1m='" + LOADAVG_1M  +
        "',loadavg_5m='" + LOADAVG_5M  + "',loadavg_15m='" + LOADAVG_15M  +
        "',get_request_num='" + getRequestNum  + "',write_success_num='" + writeSuccessNum  +
        "' where sys_uuid=" + config.server_id  + " and serial_no=" + ticket_sys_serial_num;
        console.log(sql);
        mysql1.process(sql);
        ticket_sys_serial_num ++;
      }
    }
  })
}

function dev_insert_1(){
  var mtgs = mtgStatistics;
  for(var i in mtgs){
    mtgs[i]['ASR'] = Math.round(mtgs[i]['accessTotalProcess']/mtgs[i]['totalProcess']*100) || 0;
    mtgs[i]['ACD'] = Math.round(mtgs[i]['accessTotalTime']/mtgs[i]['accessTotalProcess']) || 0;
    mtgs[i]['disconnectCause'] = JSON.stringify(mtgs[i]['disconnectCauseJSON']);
    //检测该设备数据是否大于设置值
    var sql = "select count(*) as total from tbl_dev_1 where dev_sn='" + i + "'";
    mysql1.find(sql,function(err,rows){
      if(err){
        logger.error("查询tbl_dev_1出错！");
        logger.error(err);
      }else{
        if(rows[0].total < config.ticket_count_limit){
          sql = "insert into tbl_dev_1 (rec_status,dev_sn,serial_no,create_time,asr,acd,access_total_process," +
          "access_total_time,total_process,disconnect_cause,cur_deal_status1,cur_deal_status2,cur_deal_status3," +
          "cur_deal_status4,cur_deal_status5,cur_deal_status6) values(" +
          0 + ",'" + i + "'," + ticket_dev_serial_num + ",'" + global.localStaticToUTC(new Date().getTime()) + "'," +
          mtgs[i]['ASR'] + "," + mtgs[i]['ACD'] + "," + mtgs[i]['accessTotalProcess'] + ",'"
          + mtgs[i]['accessTotalTime'] + "','" + mtgs[i]['totalProcess'] + "','" + mtgs[i]['disconnectCause'] + "','"
          + mtgs[i]['dealStatus1'] + "','" + mtgs[i]['dealStatus2'] + "','" + mtgs[i]['dealStatus3']  + "','"
          + mtgs[i]['dealStatus4'] + "','" + mtgs[i]['dealStatus5'] + "','" + mtgs[i]['dealStatus6'] + "')";
          console.log(sql);
          mysql1.process(sql);
          ticket_dev_serial_num ++;
        }else{
          if(ticket_dev_serial_num - 1 == config.ticket_count_limit){
            ticket_dev_serial_num = 1;
          }
          sql = "update tbl_dev_1 set create_time='" + global.localStaticToUTC(new Date().getTime()) + "',asr=" +
          mtgs[i]['ASR'] + ",acd=" + mtgs[i]['ACD'] + ",access_total_process=" + mtgs[i]['accessTotalProcess']
          + ",access_total_time='" + mtgs[i]['accessTotalTime'] + "',total_process='" + mtgs[i]['totalProcess']
          + "',disconnect_cause='" + mtgs[i]['disconnectCause']
          + "',cur_deal_status1='" + parseInt(mtgs[i]['dealStatus1']) + "',cur_deal_status2='" + parseInt(mtgs[i]['dealStatus2'])
          + "',cur_deal_status3='" + parseInt(mtgs[i]['dealStatus3']) + "',cur_deal_status4='" + parseInt(mtgs[i]['dealStatus4'])
          + "',cur_deal_status5='" + parseInt(mtgs[i]['dealStatus5']) + "',cur_deal_status6='" + parseInt(mtgs[i]['dealStatus6'])
          + "' where dev_sn='" + i + "' and serial_no=" + ticket_dev_serial_num;
          console.log(sql);
          mysql1.process(sql);
          ticket_dev_serial_num ++;
        }
      }
    })
  }
}

function alarm_check(){
  if(config.cpuAlarmRate != 0 && CPU_UTILIZATION > config.cpuAlarmRate){
    //判断在配置时间内无已告警未确认项，无则插入
    var sql = "select * from tbl_alarm where alarm_object=4 and confirmed=1 and create_time < DATE_SUB(NOW(),INTERVAL 30 MINUTE)";
    mysql1.find(sql,function(err,data){
      if(err){
        logger.error(err);
      }else if(!data){
        sql = "insert into tbl_alarm (level,alarm_object,alarm_cause,create_time) values(4,4,CPU_ALARM,'"
        + global.localStaticToUTC(new Date().getTime()) + "')";
        mysql1.Cb_process(sql,null,function(err){
          if(err){
            logger.error('CPU alarm error!' + err);
          }
        })
      }
    })
  }
  if(config.serverMemoryAlarmRate != 0 && SERVER_MEMORY_UTILIZATION > config.serverMemoryAlarmRate){
    //判断在配置时间内无已告警未确认项，无则插入
    var sql = "select * from tbl_alarm where alarm_object=5 and confirmed=1 and create_time < DATE_SUB(NOW(),INTERVAL 30 MINUTE)";
    mysql1.find(sql,function(err,data){
      if(err){
        logger.error(err);
      }else if(!data){
        sql = "insert into tbl_alarm (level,alarm_object,alarm_cause,create_time) values(4,5,SERVER_MEMORY_ALARM,'"
        + global.localStaticToUTC(new Date().getTime()) + "')";
        mysql1.Cb_process(sql,null,function(err){
          if(err){
            logger.error('server memory alarm error!' + err);
          }
        })
      }
    })
  }
  if(config.appMemoryAlarmRate !=0 && PROCESS_MEMORY_UTILIZATION > config.appMemoryAlarmRate){
    //判断在配置时间内无已告警未确认项，无则插入
    var sql = "select * from tbl_alarm where alarm_object=6 and confirmed=1 and create_time < DATE_SUB(NOW(),INTERVAL 30 MINUTE)";
    mysql1.find(sql,function(err,data){
      if(err){
        logger.error(err);
      }else if(!data){
        sql = "insert into tbl_alarm (level,alarm_object,alarm_cause,create_time) values(4,6,APP_MEMORY_ALARM,'"
        + global.localStaticToUTC(new Date().getTime()) + "')";
        mysql1.Cb_process(sql,null,function(err){
          if(err){
            logger.error('app memory alarm error!' + err);
          }
        })
      }
    })
  }
  if(config.diskUsedAlarmRate != 0 && DISK_UTILIZATION > config.diskUsedAlarmRate){
//判断在配置时间内无已告警未确认项，无则插入
    var sql = "select * from tbl_alarm where alarm_object=7 and confirmed=1 and create_time < DATE_SUB(NOW(),INTERVAL 30 MINUTE)";
    mysql1.find(sql,function(err,data){
      if(err){
        logger.error(err);
      }else if(!data){
        sql = "insert into tbl_alarm (level,alarm_object,alarm_cause,create_time) values(4,7,SERVER_DISK_ALARM,'"
        + global.localStaticToUTC(new Date().getTime()) + "')";
        mysql1.Cb_process(sql,null,function(err){
          if(err){
            logger.error('server disk alarm error!' + err);
          }
        })
      }
    })
  }
}

