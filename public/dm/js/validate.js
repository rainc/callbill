/**
 * Created by Rainc on 2015/6/3.
 */

$.validator.addMethod("ip", function(value, element) {
  return /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/.test(value);
});
$.validator.addMethod("password", function(value, element) {
  return /^[A-Za-z0-9\_]{6,16}$/.test(value);
});
$.validator.addMethod("userName", function(value, element) {
  return /^[A-Za-z0-9\_]{6,16}$/.test(value);
});
$.validator.addMethod("mode", function(value, element) {
  return /^[Xx0-9\+\-\[\]\{\}]*$/.test(value);
});
$.validator.addMethod("sn", function(value, element) {
  return /^([A-Fa-f0-9]{4})\-([A-Fa-f0-9]{4})\-([A-Fa-f0-9]{4})\-([A-Fa-f0-9]{4})$/.test(value);
});
$.validator.addMethod("domainName", function(value, element) {
  return /^([A-Za-z0-9]{1})([A-Za-z0-9\-]*)([A-Za-z0-9]{1})$/.test(value);
});