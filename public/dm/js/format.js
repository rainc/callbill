define(function (){
	Date.prototype.format = function(format){ 
		var o = { 
		"M+" : this.getMonth()+1, //month 
		"d+" : this.getDate(), //day 
		"h+" : this.getHours(), //hour 
		"m+" : this.getMinutes(), //minute 
		"s+" : this.getSeconds(), //second 
		"q+" : Math.floor((this.getMonth()+3)/3), //quarter 
		"S" : this.getMilliseconds() //millisecond 
		} 

		if(/(y+)/.test(format)) { 
		format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
		} 

		for(var k in o) { 
		if(new RegExp("("+ k +")").test(format)) { 
		format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
		} 
		} 
		return format; 
	} 	
	function getRealValue(name,record,value){
		if(record){
			return record[name];
		}else if(value){
			return value;
		}else{
			return "";
		}
	}
	function getDisplayValue(name,record,value){
		if(record && record[name]){
			return record[name];
		}else if(value){
			return value;
		}else{
			return "---";
		}
	}
	function getTimeValue(name,record,value){
		if(record && record[name]){
			return timeStaticFormat(record[name]);
		}else if(value){
			return timeStaticFormat(value);
		}else{
			return "---";
		}
	}
	function getDate(strDate) {
		if(!strDate){
			return "";
		}
	    var date = eval('new Date(' + strDate.replace(/\d+(?=-[^-]+$)/,
	    function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');
	    return date;
	}
	//input string;return string
//	function timeStaticFormat(value){
//		var valueD = getDate(value);
//		var da = new Date(valueD);
//		var off = valueD.getTimezoneOffset()
//		da.setHours(da.getHours()-off/60)
//		var va = da.format("yyyy-MM-dd hh:mm:ss");
// 		return va;
//
//	}
  //浏览器本地时间转UTC时间
  function localStaticToUTC(value){
    var da = new Date(value);
    var off = da.getTimezoneOffset();
    da.setHours(da.getHours()+off/60);
    var va = da.format("yyyy-MM-dd hh:mm:ss");
    return va;
  }
  function localToUTC(value,format){
    var da = new Date(value);
    var off = da.getTimezoneOffset();
    da.setHours(da.getHours()+off/60);
    var va = da.format(format);
    return va;
  }
  //UTC时间转浏览器本地时间
  function UTCStaticToLocal(value){
    //解析时间
		var da = new Date(value);
    //var timeArr = value.split('T');
    //var d =  timeArr[0].split('-');
    //var t = timeArr[1].split(':');
    //var da = new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);  //new Date转为本地时区时间
    var off = da.getTimezoneOffset();
    da.setHours(da.getHours()-off/60);
    var va = da.format("yyyy-MM-dd hh:mm:ss");
    return va;
  }
  function UTCToLocal(value,format){
    //解析时间
    //var timeArr = value.split('T');
    //var d =  timeArr[0].split('-');
    //var t = timeArr[1].split(':');
    //var da = new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);  //new Date直接转为本地时区时间
		var da = new Date(value);
    var off = da.getTimezoneOffset();
    da.setHours(da.getHours()-off/60);
    var va = da.format(format);
    return va;
  }
	//input string format;return string
//	function timeFormat(value,format){
//		var valueD = getDate(value);
//		var da = new Date(valueD);
//		var off = valueD.getTimezoneOffset()
//		da.setHours(da.getHours()-off/60)
//		var va = da.format(format);
// 		return va;
//
//	}
	function getPrivilegeImg(value){
		if(value){
//			return "<img src='/resources/images/accept.png'>";
			return '<i class="fa fa-check" style="color:#69aa46;"></i>&nbsp;';
		}else{
//			return "<img src='/resources/images/cancel.png'>";
			return '<i class="fa fa-times" style="color:#dd5a43;"></i>&nbsp;';
		}

	}
  function getErrmsg(value){
    if(typeof(value) == 'object'){
      var msg = '';
      for(var i in value){
        if(value[i][0]){
          msg += value[i] + '!'
        }else{
          continue;
        }
      }
      return msg;
    }else{
      return window.lan[value];
    }
  }
	return {
    getErrmsg:getErrmsg,
		getRealValue:getRealValue,
		getDisplayValue:getDisplayValue,
		getTimeValue:getTimeValue,
//		timeStaticFormat:timeStaticFormat,
//		timeFormat:timeFormat,
    localStaticToUTC:localStaticToUTC,
    localToUTC:localToUTC,
    UTCStaticToLocal:UTCStaticToLocal,
    UTCToLocal:UTCToLocal,
		getDate:getDate,
		getPrivilegeImg:getPrivilegeImg
	};
});


