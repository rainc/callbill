define([],function(){
	var dur, durM, val, elem, prog;
	var Pl = 0;
	var styleChange = {pause:{}, play:{}, plsbutton:{}};

	styleChange.play.change = function(){
		$('#play').addClass('hidden');
		$('#pause').removeClass('hidden');
	};

	styleChange.pause.change = function(){
		$('#pause').addClass('hidden');
		$('#play').removeClass('hidden');
	};

	styleChange.plsbutton.change = function(){
		$('#t_pls_show').addClass('selectpls');
		$('#t_pls_show').removeClass('noselectpls');
	};

	styleChange.plsbutton.recovery = function(){
		$('#t_pls_show').addClass('noselectpls');
		$('#t_pls_show').removeClass('selectpls');
	};

	function initAudio(elem) {
		//var title = elem.attr('t_name');
		//var cover = elem.attr('t_cover');
		//var artist = elem.attr('t_artist');

		//$('.title').text(' - ' + title);
		//$('.artist').text(artist);
		//$('#t_cover').html('<img src="Images/' + cover+'">');
	}

	function init(){
		window.mus = [];
		window.inited = true;

		$('#t_progress').slider({
			value: 0,
			orientation: "horizontal",
			range: "min",
			animate: true,
			step: 1
		});

		//$('audio').on("ended", function(){
		//	window.mus = $(this).parent('li').next('li').first();
		//	window.mus = window.mus.children('audio');
		//	$(this).parent("li").addClass('active');
		//	var next = $('li.active').next();
		//	$('li').removeClass('active');
		//	if(window.mus[0]){
		//		//自动播放下条录音
		//		initAudio(next);
		//		window.mus[0].play();
		//	}
		//	else{
		//		$('#error').text(window.lan['The last record!']);
		//		$('#t_cover').html('<img src="Images/logo.png">');
		//	}
		//});

		//play button
		$('#play').click(function(){
			if(window.mus){
				window.mus[0].play();
				styleChange.play.change();
				$('#error').text('');
			}
			else {
				$('#error').text(window.lan['Please select a record!']);
			}

		});

		// pause button
		$('#pause').click(function() {
			if(window.mus){
				window.mus[0].pause();
				styleChange.pause.change();
			}
			else {
				$('#error').text(window.lan['Please select a record!']);
			}

		});

		//next button
		$('#next').click(function(){
			window.mus[0].pause();
			window.mus[0].currentTime = 0;
			window.mus = window.mus.parent('li').next('li').first();
			window.mus = window.mus.children('audio');
			var next = $('li.active').next();
			$('li').removeClass('active');
			if(window.mus[0]){
				initAudio(next);
				window.mus[0].play();
			}
			else{
				$('#error').text(window.lan['It is the last record!']);
				//window.mus = null;
			}
		});

		//prev button
		$('#prev').click(function(){
			window.mus[0].pause();
			window.mus[0].currentTime = 0;
			window.mus = window.mus.parent('li').prev('li').last();
			window.mus = window.mus.children('audio');
			var prev = $('li.active').prev();
			$('li').removeClass('active');
			if(window.mus[0]){
				initAudio(prev);
				window.mus[0].play();
			}
			else{
				$('#error').text(window.lan['It is the first record!']);
				//window.mus = null;
			}
		});

		//volume
		$('#rangeVal').slider({
			value: 60,
			orientation: "horizontal",
			range: "min",
			animate: true,
			step: 1
		});

		// volume text
		val = $('#rangeVal').slider("value");
		$('#val').text(val);

		var tooltip = $('#val');
		tooltip.hide();

		$('#rangeVal').slider({
			start: function( event, ui ) {
				tooltip.fadeIn('fast');
			},
			stop: function(event,ui) {
				tooltip.fadeOut('fast');
			},
			slide: function( event, ui ) {
				val = ui.value;
				tooltip.css('left', val-30).text(ui.value);
				$('#val').text(val);

				if(window.mus && window.mus[0]){
					window.mus[0].volume = val/100;
				}
				else {
					$('#error').text(window.lan['Please select a record!']);
				}
			}
		});

		// progress
		$('#t_progress').slider({
			start: function( event, ui ) {
				if(window.mus && window.mus[0]){
					window.mus[0].pause();
				}
			},
			stop: function( event, ui ) {
				if(window.mus && window.mus[0]){
					prog = ui.value;
					window.mus[0].currentTime = prog;
					window.mus[0].play();
					styleChange.play.change();
				}
			}
		});

		//playlist button
		$('#t_pls_show').click(function(){
			if (Pl == 0) {
				styleChange.plsbutton.change();
				Pl = 1;
			}
			else {
				styleChange.plsbutton.recovery();
				Pl = 0;
			}
			$('#playlist').slideToggle();
		});
	};
	function initPlaylist(){
		$('#playlist ul li a').click(function(){
			initAudio($(this).parent("li"));
			$('#error').text('');
			styleChange.play.change();
			console.log(window.mus);
			if(window.mus){
				//先判断是否有正在播放的录音，有则停掉
				if(window.mus.length > 0){
					window.mus[0].pause();
					window.mus[0].currentTime = 0;
					$('li').removeClass('active');
				}
			}
			window.mus = $(this).next("audio");
			$(this).parent("li").addClass('active');
			window.mus[0].play();
		});
		$('audio').on("timeupdate", function() {
			window.mus[0].volume = val/100;
			d = this.duration;
			c = this.currentTime;
			curM = Math.floor(c/60);
			curS = Math.round(c - curM*60);
			$('#current').text(curM + ':' + curS);
			$('#t_progress').slider({
				max: d,
				min: 0,
				value: c
			});
		});

		$('audio').on("playing", function () {
			dur = this.duration;
			durM = Math.floor(dur/60) + ':' + Math.round(dur - Math.floor(dur/60) * 60);
			$('#duration').text(durM);
			$(this).parent("li").addClass('active');
		});
	}

	return {
		init: init,
		initPlaylist:initPlaylist
	}
})

