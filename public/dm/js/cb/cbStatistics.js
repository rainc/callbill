/**
 * Created by Rainc on 2015/9/15.
 */
define(['cb-fun','bootstrap-table'],function(cf){
  function createStatisticList(pid,id){
    var pn=$("#"+pid);
    if(!pn){
      return;
    }
    var html =  '<div id="'+id+'-toolbar" class="btn-group" role="group" aria-label="...">';
    html+='</div>'
    html+='<table id='+id+'></table>';
    pn.html("");
    pn.append(html);

    createColEvents(pid,id);

    $('#'+id).bootstrapTable({
      method: 'post',
      contentType:'application/x-www-form-urlencoded;charset=UTF-8',
      url: '/api',
      cache: false,
      responseHandler:function(res){
        var obj={};
        if(res && res.result.list && res.result.list.length>0){
          obj["rows"]=res.result.list;
          obj.total=res['result']["total"];
          return obj;
        }else{
          obj["rows"]= [];
          obj.total=res['result']["total"];
          return obj;
        }
      },
      queryParams:function(p){
        p.action = 'getDevStatistic';
        return p;
      },
      height:515,
      striped: true,
      pagination: true,
      sortOrder: 'desc',
      toolbar:"#"+id+"-toolbar",
      pageSize: 10,
      pageList: [10, 50,200,500,1000,'All'],
      search: true,
      showColumns: true,
      showRefresh: true,
      showToggle:true,
      queryParamsType:'limit',
      sidePagination: "server",
      smartDisplay:true,
      minimumCountColumns: 2,
      undefinedText: '-',
      clickToSelect:true,
      columns: [{
        field: 'state',
        checkbox: true,
        valign: 'middle'
      },{
        field: 'uuid',
        title: 'id',
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: false
      },{
        field: 'dev_sn',
        title: window.lan['dev sn'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'asr',
        title: window.lan['ASR'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'acd',
        title: window.lan['ACD'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'access_total_process',
        title: window.lan['access total process'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'access_total_time',
        title: window.lan['access total time'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'total_process',
        title: window.lan['total process'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'cur_deal_status1',
        title: window.lan['Forbidden to call'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'cur_deal_status2',
        title: window.lan['Forbidden to through'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'cur_deal_status3',
        title: window.lan['Forbidden to call and through'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'cur_deal_status4',
        title: window.lan['Record'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'cur_deal_status5',
        title: window.lan['Frequency control'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'cur_deal_status6',
        title: window.lan['Normal communication'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'create_time',
        title: window.lan['create time'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true,
        formatter:function(value,row,index){
          if(value){
            return window.format.UTCStaticToLocal(value);
          }
          return "---";
        }
      },{
        field: 'disconnect_cause',
        title: window.lan['disconnect cause'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        clickToSelect: true,
        formatter:function(value,row,index){
          var html='';
          html += '<a action="check" class="blue" href="#" title="' + window.lan['check'] + '">'
          +'<button ><i class="fa fa-search"></i>' + window.lan['check'] + '</button>'
          +'</a>'
          +'&nbsp;';
          return html;
        },
        events:operateEvents
      }]
    });
    window.list.changeForAce(pid);
    $(window).resize(function () {
      window.list.changeView(pid,id,600);
    });
    window.list.changeView(pid,id,600);
  }
  function createColEvents(pid,id){
    var check=function (e, value, row, index) {
      checkDisconnectCause(id,[row]);
    };
    window.operateEvents = {
      'click a[action=check]':check
    };
  }
  function checkDisconnectCause(tid,row){
    console.log(row[0].disconnect_cause);
    var pn=$("#myModal");
    if(!pn) return;
    var html="<div class='modal-dialog'>" +
      "<div class='modal-content'>" +
      "<div class=\"modal-header\">" +
      "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>" +
      "<h4 class=\"modal-title\" id=\"myModalLabel\">" + window.lan['disconnect cause detail'] + "</h4></div>"
      +'<div class="modal-body">'
      +'<table class="" id="disconnectCauseTable" >';
    html+='</table>'
      +'</div>' +
      "<div class=\"modal-footer\"><button name=\"close\" type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">" + window.lan['cancel'] + "</button>" +
      "</div></div><!-- /.modal-content --></div>"
    pn.html("");
    pn.append(html);

    $('#myModal').modal().css({
      width: 'auto',
      backdrop:false
    });
    $('#disconnectCauseTable').bootstrapTable({
      data:JSON.parse(row[0].disconnect_cause),
      pageSize: 10,
      columns: [{
        field: 'name',
        title: window.lan['name'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true,
        formatter:function(value,row,index){
          if(value){
            return window.lan[value];
          }
          return "---";
        }
      },{
        field: 'value',
        title: window.lan['count'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      }]
    })
  }
  return{
    createStatisticList:createStatisticList
  }
})
