/**
 * Created by Rainc on 2015/9/14.
 */
define(["form-field"],function(field){
  function createConfigurationPanel(pid,id,obj){
    var pn=$("#"+pid);
    if(!pn) return;
    var html='<form id="'+id+'" class="my-tab" role="form">'
      +'<div class="btn-group">'
      +'<button type="button"  name="save" title="' + window.lan['save'] +'" class="btn btn-success btn-sm tooltip-success"><i class="fa fa-save bigger-100"></i>&nbsp;' + window.lan['save'] +'</button>'
      +'<button name="refresh" type="button" title="' + window.lan['refresh'] +'" class="btn btn-warning btn-sm tooltip-warning"><i class="fa fa-refresh bigger-100"></i>&nbsp;' + window.lan['refresh'] +'</button>'
      +'<button name="restartNow" type="button" title="' + window.lan['restart now'] +'" class="btn btn-danger btn-sm tooltip-warning"><i class="fa fa-history bigger-100"></i>&nbsp;' + window.lan['restart now'] +'</button>'
      +'</div>'
      +'<div class="container-fluid" style="margin-top: 20px">'
      +'<div class="row">'

      +'<div class="col-md-6">'
      +'<div name="basic" ><h4 style="font-weight: bold">' + window.lan['Basic Configuration'] + '</h4>'
      +field.getTextField("server_ip","",window.lan['server ip'],"","",true)
      +field.getTextField("web_port","",window.lan['web port'],"","",true)
      +field.getTextField("db_host","",window.lan['db host'],"","",true)
      +field.getTextField("db_user","",window.lan['db user'],"","",true)
      +field.getTextField("db_passwd","",window.lan['db password'],"","",true)
      +field.getTextField("db_database","",window.lan['db database'],"","",true)
      +'</div>'
      +'</div>'
      +'<div class="col-md-6">'
      +'<div name="alarmRate" ><h4 style="font-weight: bold">' + window.lan['Alarm rate Configuration']
      + ' (' + window.lan['No alarm when it is zero!'] + ')' + '</h4>'
      +field.getTextField("cpuAlarmRate","",window.lan['cpu alarm rate(%)'])
      +field.getTextField("serverMemoryAlarmRate","",window.lan['server memory alarm rate(%)'])
      +field.getTextField("appMemoryAlarmRate","",window.lan['app memory alarm rate(%)'])
      +field.getTextField("diskUsedAlarmRate","",window.lan['disk used alarm rate(%)'])
      +'</div>'
      +'</div>'

      +'</div>'
      +'</div>'
      +'</form>';
    pn.html("");
    pn.append(html);

    $('#'+id).autofill(obj);

    //表单验证
    $('#'+id).validate({
      rules: {
        server_ip:{
          required:true,
          ip:true
        },
        web_port:{
          required:true,
          digits:true,
          range:[0,65535]
        },
        db_host:{
          required:true,
          ip:true
        },
        db_user:{
          required:true
        },
        db_passwd:{
          required:true
        },
        db_database:{
          required:true
        },
        cpuAlarmRate:{
          required:true,
          digits:true,
          range:[0,100]
        },
        serverMemoryAlarmRate:{
          required:true,
          digits:true,
          range:[0,100]
        },
        appMemoryAlarmRate:{
          required:true,
          digits:true,
          range:[0,100]
        },
        diskUsedAlarmRate:{
          required:true,
          digits:true,
          range:[0,100]
        }
      },
      messages: {
        server_ip:{
          required:window.lan['server ip is required!'],
          ip:window.lan['ip_valid']
        },
        web_port:{
          required:window.lan['web port is required!'],
          digits: window.lan['port_range'],
          range: window.lan['port_range']
        },
        db_host:{
          required:window.lan['db host is required!'],
          ip: window.lan['ip_valid']
        },
        db_user:{
          required:window.lan['db user is required!']
        },
        db_passwd:{
          required:window.lan['db password is required!']
        },
        db_database:{
          required:window.lan['db database is required!']
        },
        cpuAlarmRate:{
          required:window.lan['CPU alarm rate is required!'],
          digits: window.lan['rate_range'],
          range: window.lan['rate_range']
        },
        serverMemoryAlarmRate:{
          required:window.lan['server memory alarm rate is required!'],
          digits: window.lan['rate_range'],
          range: window.lan['rate_range']
        },
        appMemoryAlarmRate:{
          required:window.lan['app memory alarm rate is required!'],
          digits: window.lan['rate_range'],
          range: window.lan['rate_range']
        },
        diskUsedAlarmRate:{
          required:window.lan['disk used alarm rate is required!'],
          digits: window.lan['rate_range'],
          range: window.lan['rate_range']
        }
      }
    });

    $("#"+id+" button[name=save]").bind('click',function(){
      //检查是否通过验证
      if($('#' + id).valid()) {
        var form = $("#" + id);
        var pa = form.formSerialize();
        pa += '&action=saveServerConfiguration';
        $.ajax({
          url: "/api",
          type: "POST",
          data: pa,
          complete: function (data, str) {
            if (data.status == 200) {
              if (data.responseJSON && data.responseJSON.success) {
                $("#"+id+" button[name=refresh]").trigger('click');
                window.tip.show_pk("success", null, window.lan['Save server configuration success!']);
              } else {
                window.tip.show_pk("danger", null, window.lan['Save server configuration failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
              }
            } else {
              window.tip.show_pk("danger", null, window.lan['Network error!']);
            }
          }});
      }else{
        window.tip.show_pk("danger",null,window.lan['Validation failed!']);
      }
    });

    $("#"+id+" button[name=restartNow]").bind('click',function(e){
      window.tip.show_pk("info", 60, window.lan['server restarting...please refresh later!'],true);
      $.ajax({
        url: "/api",
        type: "POST",
        data: {action:'restartNow'},
        complete: function (data, str) {
          if (data.status == 200) {
            if (data.responseJSON && data.responseJSON.success) {
              window.tip.show_pk("success", null, window.lan['server restart success!Please refresh page!']);
            } else {
              if(data.responseJSON.errMsg == 'Session timeout!Please relogin!'){
                window.location.reload();
              }else{
                window.tip.show_pk("danger", null, window.lan['server restart failed!'] +
                window.format.getErrmsg(data.responseJSON.errMsg));
              }
            }
          } else {
            //服务重启后刷新页面
            setTimeout(function(){
              window.location.reload();
            },3000)
            //window.tip.show_pk("danger", null, window.lan['Network error!']);
          }
        }});
    })

  }
  function loadLocalData(pid,id,obj){
    createConfigurationPanel(pid,id,obj);
  }
  function loadRemoteData(pid,id){
    $.ajax({
      url:"/api",
      data:{action:'getServerConfiguration'},
      type:'post',
      complete:function(data,str){
        if(data && data.status == 200){
          if(data.responseJSON && data.responseJSON.success){
            loadLocalData(pid,id,data.responseJSON.result);
            $("#"+id+" button[name=refresh]").bind('click',function(){
              loadRemoteData(pid,id);
            });
          }else{
            var errMsg = data.responseJSON.errMsg;
            if(errMsg == 'Session timeout!Please relogin!'){
              window.location.href = '/'
            }else{
              window.tip.show_pk("danger",null,window.lan['Get server configuration failed!'] + window.format.getErrmsg(data.responseJSON.errMsg))
            }
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['Network error!']);
        }
      }})
  }
  return{
    loadRemoteData:loadRemoteData
  }
})