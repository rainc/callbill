/**
 * Created by Rainc on 2015/9/9.
 */
define([],function (){
  function createHtml(pid){
    //var pn=$("#main_search");
    //pn.html("");

    createTabHtml(pid);
  }
  function createTabHtml(pid){
    var pn=$("#"+pid);
    pn.html("");
    var html='<ul id="myTab" class="nav nav-tabs">'
      +'<li role="presentation"><a href="#callBill_config"><i class="fa fa-cog"></i>&nbsp;'+window.lan["callBillConifg"]+'</a></li>'
      +'<li role="presentation"><a href="#callBill_info"><i class="fa fa-cog"></i>&nbsp;'+window.lan["callBillInfo"]+'</a></li>'
      +'<li role="presentation"><a href="#callBill_alarm"><i class="fa fa-cog"></i>&nbsp;'+window.lan["callBillAlarm"]+'</a></li>'
      +'<li role="presentation"><a href="#callBill_performance"><i class="fa fa-cog"></i>&nbsp;'+window.lan["callBillPerformance"]+'</a></li>'
      +'<li role="presentation"><a href="#callBill_statistics"><i class="fa fa-cog"></i>&nbsp;'+window.lan["callBillStatistics"]+'</a></li>'
      +'</ul>'
      +'<div  class="tab-content">'
      +'<div class="tab-pane fade in"  id="callBill_config" >'
      +'</div>'
      +'<div class="tab-pane fade in"  id="callBill_info" >'
      +'</div>'
      +'<div class="tab-pane fade in"  id="callBill_alarm" >'
      +'</div>'
      +'<div class="tab-pane fade in"  id="callBill_performance" >'
      +'</div>'
      +'<div class="tab-pane fade in"  id="callBill_statistics" >'
      +'</div>'
      +'</div>';
    pn.append(html);
  }
  function tabAfterShow(id){
    if(id=="#callBill_info"){
      require(["cb-info"], function(ci) {
        ci.createCallInfo(id.substring(1),id.substring(1)+"_child");
      });
    }else if(id == '#callBill_config'){
      require(["cb-config"], function(cc) {
        cc.loadRemoteData(id.substring(1),id.substring(1)+"_child");
      });
    }else if(id == '#callBill_alarm'){
      require(["cb-alarm"], function(ca) {
        ca.createAlarmLogList(id.substring(1),id.substring(1)+"_child");
      });
    }else if(id == '#callBill_performance'){
      require(["cb-performance"], function(cp) {
        cp.loadRemoteData(id.substring(1),id.substring(1)+"_child");
      });
    }else if(id == '#callBill_statistics'){
      require(["cb-statistics"], function(cp) {
        cp.createStatisticList(id.substring(1),id.substring(1)+"_child");
      });
    }
  }
  function procTab(){
    $('#myTab a').bind("shown.bs.tab",function(){
      var id=$(this).attr("href");
      tabAfterShow(id);
    });

    $('#myTab a').click(function(e) {
      e.preventDefault()
      $(this).tab('show');
    });

    setTimeout(function(){
      $('#myTab a:eq(0)').trigger("click");
    },300)
  }
  function init(){
    window.tabAfterShow=tabAfterShow;
    createHtml("my-tab-position");
    procTab();
  }
  return {
    createHtml:createHtml,
    createTabHtml:createTabHtml,
    procTab:procTab,
    init:init,
    tabAfterShow:tabAfterShow
  };
});