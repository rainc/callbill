/**
 * Created by Rainc on 2015/9/15.
 */
define(['echarts-mobile'],function(){
  function createPerformanceStatistic(pid,id,obj,cb){
    require.config({
      paths: {
        "echarts": "echarts-m-1.0.0/source"
      }
    });
    require(
      [
        'echarts',
        'echarts/chart/line'
      ],
      function (ec) {
        window.performance_statistic_line = ec.init(document.getElementById(id));
        option = {
          tooltip : {
            trigger: 'axis'
          },
          title: {
            text: window.lan['Comprehensive performance statistics(%)'],
            x:'left',
            y:'top',
            textStyle:{
              //fontSize: 14,
//						    fontWeight: 'bolder',
              color: '#707070'
            }
          },
          legend: {
            textStyle:{fontSize:10},
            data:[window.lan['cpu userate'],window.lan['server memory userate'],window.lan['disk userate'],
              window.lan['app memory userate'],window.lan['loadavg1'],window.lan['loadavg5'],window.lan['loadavg15']]
          },
          dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
          },
          xAxis : [
            {
              type : 'category',
              boundaryGap : false,
              data : function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(window.format.UTCStaticToLocal(obj[i]["create_time"]));
                  }
                }
                return list;
              }()
            }
          ],
          yAxis : [
            {
              type : 'value'
            }
          ],
          series : [
            {
              name: window.lan['cpu userate'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["server_cpu_userate"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['server memory userate'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["server_memory_userate"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['disk userate'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["server_disk_userate"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['app memory userate'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["app_memory_userate"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['loadavg1'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["loadavg_1m"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['loadavg5'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["loadavg_5m"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['loadavg15'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["loadavg_15m"]);
                  }
                }
                return list;
              }()
            }
          ]
        };
        window.performance_statistic_line .setOption(option);
        $(window).resize(function(){
          window.performance_statistic_line .resize();
        });
        cb();
      }
    );
  }
  function createRequestStatistic(pid,id,obj,cb){
    require.config({
      paths: {
        "echarts": "echarts-m-1.0.0/source"
      }
    });
    require(
      [
        'echarts',
        'echarts/chart/line'
      ],
      function (ec) {
        window.request_statistic_line = ec.init(document.getElementById(id));
        option = {
          tooltip : {
            trigger: 'axis'
          },
          title: {
            text: window.lan['Comprehensive quest statistics'],
            x:'left',
            y:'top',
            textStyle:{
              //fontSize: 14,
//						    fontWeight: 'bolder',
              color: '#707070'
            }
          },
          legend: {
            textStyle:{fontSize:10},
            data:[window.lan['get request num'],window.lan['write success num']]
          },
          dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
          },
          xAxis : [
            {
              type : 'category',
              boundaryGap : false,
              data : function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(window.format.UTCStaticToLocal(obj[i]["create_time"]));
                  }
                }
                return list;
              }()
            }
          ],
          yAxis : [
            {
              type : 'value'
            }
          ],
          series : [
            {
              name: window.lan['get request num'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["get_request_num"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['write success num'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["write_success_num"]);
                  }
                }
                return list;
              }()
            }
          ]
        };
        window.request_statistic_line .setOption(option);
        $(window).resize(function(){
          window.request_statistic_line .resize();
        });
        cb();
      }
    );
  }
  function createDelayStatistic(pid,id,obj,cb){
    require.config({
      paths: {
        "echarts": "echarts-m-1.0.0/source"
      }
    });
    require(
      [
        'echarts',
        'echarts/chart/line'
      ],
      function (ec) {
        window.delay_statistic_line = ec.init(document.getElementById(id));
        option = {
          tooltip : {
            trigger: 'axis'
          },
          title: {
            text: window.lan['Comprehensive delay statistics'],
            x:'left',
            y:'top',
            textStyle:{
              //fontSize: 14,
//						    fontWeight: 'bolder',
              color: '#707070'
            }
          },
          legend: {
            textStyle:{fontSize:10},
            data:[window.lan['agent avg delay'],window.lan['lasting avg delay']]
          },
          dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
          },
          xAxis : [
            {
              type : 'category',
              boundaryGap : false,
              data : function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(window.format.UTCStaticToLocal(obj[i]["create_time"]));
                  }
                }
                return list;
              }()
            }
          ],
          yAxis : [
            {
              type : 'value'
            }
          ],
          series : [
            {
              name: window.lan['agent avg delay'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["agent_avg_delay"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['lasting avg delay'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["lasting_avg_delay"]);
                  }
                }
                return list;
              }()
            }
          ]
        };
        window.delay_statistic_line .setOption(option);
        $(window).resize(function(){
          window.delay_statistic_line .resize();
        });
        cb();
      }
    );
  }
  function loadLocalData(pid,obj){
    var pn=$("#"+pid);
    if(!pn) return;
    var html='<div class="my-tab"><div class="container-fluid" >'
      +'<div class="row" >'
      +'<div class="col-md-12" style="margin-top: 0px">'
      +'<div style="width:100%">'
      +'<span id="performance_line" style="display:inline-block;height:250px;width:100%"></span>'
      +'<span id="request_line" style="display:inline-block;height:250px;width:100%"></span>'
      +'<span id="delay_line" style="display:inline-block;height:250px;width:100%"></span>'
      +'</div>'
      +'</div>'
      +'</div>'
      +'</div></div>';

    pn.html("");
    pn.append(html);

    //生成综合历史统计折线图
    createPerformanceStatistic(pid,'performance_line',obj,function(){
      //生成综合请求统计折线图
      createRequestStatistic(pid,'request_line',obj,function(){
        //生成综合延时统计折线图
        createDelayStatistic(pid,'delay_line',obj,function(){
          //实现多图联动
          window.performance_statistic_line.connect([window.request_statistic_line,window.delay_statistic_line]);
          window.request_statistic_line.connect([window.performance_statistic_line,window.delay_statistic_line]);
          window.delay_statistic_line.connect([window.performance_statistic_line,window.request_statistic_line]);
        });
      });
    });

  }
  function loadRemoteData(pid,id){
    showPerformanceStatistic(pid);

    //设置定时器，每60秒刷新一次
    window['performance_statistic_timer'] = setInterval(function(){
      //判断performance_statistic是否当前激活的tab，否则清除定时器
      if($("a[href='#callBill_performance']").parent().attr("class")!= "active"){
        clearInterval(window['performance_statistic_timer'])
        return;
      }
      $.ajax({
        url:'/api',
        data:{action:'getPerformanceCurrentStatistic'},
        type:'POST',
        complete:function(data,str){
          if(data && data.status == 200){
            if(data.responseJSON && data.responseJSON.success){
              var performanceObj = [],requestObj = [],delayObj = [],obj=data.responseJSON.result;
              var statusLineData = window.performance_statistic_line.getSeries();
              var keepLineData = false;
              //判断数据量是否超过一个星期的量，若是则不改变数据量，否则增加数据量
              if(statusLineData[0].data.length < 60*24*7) {
                keepLineData = true
              }
              for(var i =0;i< obj.length;i++){
                performanceObj.push([0,obj[i]['server_cpu_userate'],false,keepLineData,window.format.UTCStaticToLocal(obj[i]["create_time"])]);
                performanceObj.push([1,obj[i]['server_memory_userate'],false,keepLineData]);
                performanceObj.push([2,obj[i]['server_disk_userate'],false,keepLineData]);
                performanceObj.push([3,obj[i]['app_memory_userate'],false,keepLineData]);
                performanceObj.push([4,obj[i]['loadavg1'],false,keepLineData]);
                performanceObj.push([5,obj[i]['loadavg5'],false,keepLineData]);
                performanceObj.push([6,obj[i]['loadavg15'],false,keepLineData]);
                requestObj.push([0,obj[i]['getRequestNum'],false,keepLineData,window.format.UTCStaticToLocal(obj[i]["create_time"])]);
                requestObj.push([0,obj[i]['writeSuccessNum'],false,keepLineData]);
                delayObj.push([0,obj[i]['agentAvgDelay'],false,keepLineData,window.format.UTCStaticToLocal(obj[i]["create_time"])]);
                delayObj.push([0,obj[i]['lastingAvgDelay'],false,keepLineData]);
              }
              window.performance_statistic_line.addData(performanceObj);
              window.request_statistic_line.addData(requestObj);
              window.delay_statistic_line.addData(delayObj);
            }else{
              window.tip.show_pk("danger",null,window.lan['Failed to get the performance statistic!'] + window.format.getErrmsg(data.responseJSON.errMsg));
            }
          }else{
            window.tip.show_pk("danger",null,window.lan['Network error!']);
          }
        }
      })
    }, 60 *1000)
  }
  function showPerformanceStatistic(pid){
    $.ajax({
      url:'/api',
      data:{action:'getPerformanceStatistic'},
      type:'POST',
      complete:function(data,str){
        if(data && data.status == 200){
          if(data.responseJSON && data.responseJSON.success){
            var obj=data.responseJSON.result;
            loadLocalData(pid,obj);
          }else{
            window.tip.show_pk("danger",null,window.lan['Failed to get the performance statistic!'] + window.format.getErrmsg(data.responseJSON.errMsg));
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['Network error!']);
        }
      }
    })
  }
  return {
    createPerformanceStatistic:createPerformanceStatistic,
    loadRemoteData:loadRemoteData
  }
})