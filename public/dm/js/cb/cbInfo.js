/**
 * Created by Rainc on 2015/9/9.
 */
define(["form-field","cb-fun","tPlayer","bootstrap-table"],function(field,cf,tp){
  function createCallInfo(pid,id){
    var pn=$("#"+pid);
    if(!pn){
      return;
    }
    var html =  field.getSellectText(id + '_label_name','1',[{value:0,text:''},{value:1,text:window.lan['disconnect_cause']}
      ,{value:2,text:window.lan['caller']},{value:3,text:window.lan['called']}
      ,{value:4,text:window.lan['mtgSN']},{value:5,text:window.lan['authAction']}],id+ '_select_name','1',[{value:0,text:''}
      ,{value:1,text:'='},{value:2,text:'!='}],id + '_text_name','','',['col-md-6']);
      html += '<div id="'+id+'-toolbar" ><div class="btn-group" role="group" aria-label="...">'
      + '<button id="'+id+'-query" class="btn btn-success btn-sm" title="' + window.lan['query'] + '"><i class="fa fa-search bigger-100"></i>&nbsp;' + window.lan['query'] + '</button>'
      + '<button id="'+id+'-del" class="btn btn-danger btn-sm" title="' + window.lan['delete'] + '"><i class="fa fa-minus-square bigger-100"></i>&nbsp;' + window.lan['delete'] + '</button>'
      + '<button id="'+id+'-export" class="btn btn-info btn-sm" title="' + window.lan['export'] + '"><i class="fa fa-sign-out bigger-130"></i>&nbsp;' + window.lan['export'] + '</button>'
      + '<button id="'+id+'-empty" class="btn btn-danger btn-sm" title="' + window.lan['empty'] + '"><i class="fa fa-trash bigger-100"></i>&nbsp;' + window.lan['empty'] + '</button>'
      + '</div></div>';
    html+='<table id='+id+'></table>';
    pn.html("");
    pn.append(html);

    createColEvents(pid,id);

    $('#'+id).bootstrapTable({
      method: 'post',
      contentType:'application/x-www-form-urlencoded;charset=UTF-8',
      url: '/api',
      cache: false,
      responseHandler:function(res){
        var obj={};
        if(res && res.result.list && res.result.list.length>0){
          obj["rows"]=res.result.list;
          obj.total=res['result']["total"];
          return obj;
        }else{
          obj["rows"]= [];
          obj.total=res['result']["total"];
          return obj;
        }
      },
      queryParams:function(p){
        var labelValue = $('select[name=' + id + '_label_name]').val();
        var selectValue = $('select[name=' + id + '_select_name]').val();
        var textValue = $('input[name=' + id + '_text_name]').val();
        if(labelValue != 0 && selectValue != 0 && textValue!= ''){
          p['labelValue'] = labelValue;
          p['selectValue'] = selectValue;
          p['textValue'] = textValue;
        }
        p.action = 'getCallInfo';
        return p;
      },
      height:515,
      striped: true,
      pagination: true,
      toolbar:"#"+id+"-toolbar",
      pageSize: 10,
      pageList: [10, 50,200,500,1000,'All'],
      search: false,
      showColumns: true,
      showRefresh: true,
      showToggle:true,
      queryParamsType:'limit',
      sidePagination: "server",
      smartDisplay:true,
      minimumCountColumns: 2,
      undefinedText: '-',
      clickToSelect:true,
      sortName: 'recv_time',
      sortOrder: 'desc',
      columns: [{
        field: 'state',
        checkbox: true,
        valign: 'middle'
      },{
        field: 'uuid',
        title: 'id',
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: false
      },{
        field: 'domain_uuid',
        title: window.lan['domain_uuid'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'caller',
        title: window.lan['caller'],
        align: 'center',
        valign: 'middle',
        sortable: true
      },{
        field: 'called',
        title: window.lan['called'],
        align: 'center',
        valign: 'middle',
        sortable: true
      },{
        field: 'session_time',
        title: window.lan['session_time'],
        align: 'center',
        valign: 'middle',
        sortable: true
      },{
        field: 'disconnect_cause',
        title: window.lan['disconnect_cause'],
        align: 'center',
        valign: 'middle',
        sortable: true
      },{
        field: 'mtgSN',
        title: window.lan['mtgSN'],
        align: 'center',
        valign: 'middle',
        sortable: true
      },{
        field: 'authAction',
        title: window.lan['authAction'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value == '1'){
            return '禁呼'
          }else if(value == '2'){
            return '禁透'
          }else if(value == '3'){
            return '禁呼禁透'
          }else if(value == '4'){
            return '录音'
          }else if(value == '5'){
            return '频率控制'
          }
          return "---";
        }
      },{
        field: 'report_time',
        title: window.lan['report_time'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: false,
        formatter:function(value,row,index){
          if(value && value!= '0000-00-00 00:00:00'){
            return window.format.UTCStaticToLocal(value);
          }
          return "---";
        }
      },{
        field: 'setup_time',
        title: window.lan['setup_time'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value  && value!= '0000-00-00 00:00:00'){
            return window.format.UTCStaticToLocal(value);
          }
          return "---";
        }
      },{
        field: 'alert_time',
        title: window.lan['alert_time'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value && value!= '0000-00-00 00:00:00'){
            return window.format.UTCStaticToLocal(value);
          }
          return "---";
        }
      },{
        field: 'connect_time',
        title: window.lan['connect_time'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value && value!= '0000-00-00 00:00:00'){
            return window.format.UTCStaticToLocal(value);
          }
          return "---";
        }
      },{
        field: 'disconnect_time',
        title: window.lan['disconnect_time'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value && value!= '0000-00-00 00:00:00'){
            return window.format.UTCStaticToLocal(value);
          }
          return "---";
        }
      },{
        field: 'recv_time',
        title: window.lan['recv_time'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value && value!= '0000-00-00 00:00:00'){
            return window.format.UTCStaticToLocal(value);
          }
          return "---";
        }
      },{
        field: '',
        title: '',
        align: 'left',
        valign: 'middle',
        sortable: true,
        clickToSelect: true,
        formatter:function(value,row,index){
          var html='';
          html += '<a action="play" class="blue" href="#" title="' + window.lan['play'] + '">'
          +'<i class="fa fa-play-circle bigger-130"></i>'
          +'</a>'
          +'&nbsp;';
          return html;
        },
        events:operateEvents
      }]
    });
    window.list.changeForAce(pid);
    $(window).resize(function () {
      window.list.changeView(pid,id,600);
    });
    window.list.changeView(pid,id,600);
    $("#"+ id +"-del").click(function(e){
      delItem(id);
    })
    $("#"+ id +"-export").click(function(e){
      exportItem(id);
    })
    $("#"+ id +"-empty").click(function(e){
      emptyItem(id);
    })
    $("#"+ id +"-query").click(function(e){
      searchCallBill(id);
    })
  }
  function createColEvents(pid,id){
    var play=function (e, value, row, index) {
      playItem(id,[row]);
    };
    window.operateEvents = {
      'click a[action=play]':play
    };
  }
  function playItem(id,row){
    //查询符合条件的录音文件，生成列表
    $.ajax({
      url: "/api",
      data: {action: 'getValidRecord',recordId: row[0].recordId,caller:row[0].caller,called:row[0].called},
      type:"post",
      complete: function(data,str){
        if(data.status == 200){
          if(data.responseJSON && data.responseJSON.success){
            window.tip.show_pk("success",null,window.lan['Query record file success!'] );
            var list = data.responseJSON.result.list;
            var ip = data.responseJSON.result.ip;
            var port = data.responseJSON.result.port;
            //构建ul列表的html
            var html = '';
            for(var i =0;i<list.length;i++){
              html += '<li t_cover="goldfrapp.jpg" t_artist="" t_name=""><a href="#">' + list[i].fileName + '</a>' +
                '<audio preload="auto"><source src="http://' + ip + ':' + port + '/' + list[i].fileDir + '/' + list[i].fileName + '" type="audio/ogg">' +
                '</audio></li>'
            }
            $('#playlist ul').html(html);
            //初始化录音播放器
            if(!window.inited){
              tp.init();
            }
            tp.initPlaylist();
            //判断列表是否打开状态，若否，则打开
            if($('#t_pls_show.selectpls').length == 0){
              $('#t_pls_show').trigger('click');
            }
          }else{
            window.tip.show_pk("danger",null,window.lan['Query record file failed!'] +  window.format.getErrmsg(data.responseJSON.errMsg));
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['Network error!']);
        }
      }});
  }
  function searchCallBill(tid){
    var labelValue = $('select[name=' + tid + '_label_name]').val();
    var selectValue = $('select[name=' + tid + '_select_name]').val();
    var textValue = $('input[name=' + tid + '_text_name]').val();
    var dataValue = {action:'getCallInfo',order:'desc',limit:'10',offset:'0'};
    if(labelValue != 0 && selectValue != 0 && textValue!= ''){
      dataValue['labelValue'] = labelValue;
      dataValue['selectValue'] = selectValue;
      dataValue['textValue'] = textValue;
    }
    $.ajax({
      url: "/api",
      data: dataValue,
      type:"post",
      complete: function(data,str){
        if(data.status == 200){
          if(data.responseJSON && data.responseJSON.success){
            window.tip.show_pk("success",null,window.lan['Query call info success!'] );
            var obj={};
            if(data.responseJSON && data.responseJSON.result.list && data.responseJSON.result.list.length>0){
              obj["rows"]=data.responseJSON.result.list;
              obj.total=data.responseJSON['result']["total"];
            }else{
              obj["rows"]= [];
              obj.total=data.responseJSON['result']["total"];
            }
            $('#'+tid).bootstrapTable('load',obj);
          }else{
            window.tip.show_pk("danger",null,window.lan['Query call info failed!'] +  window.format.getErrmsg(data.responseJSON.errMsg));
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['Network error!']);
        }
      }});
  }
  function delItem(tid){
    var rows=$('#'+tid).bootstrapTable('getSelections');
    if(rows.length==0) {
      window.tip.show_pk("warning", null, window.lan['You must select a item!']);
      return;
    }else {
      var items = [];
      for (var i = 0; i < rows.length; i++) {
        items.push(rows[i].uuid);
      }
      cf.createDelItemConfirm(function(){
        $('#myModal').modal().css({
          width: 'auto',
          backdrop:false
        });
        $('#myModal button[name=commit]').bind("click",function(){
          $('#myModal button[name=close]').trigger("click");
          window.tip.show_pk("info",60,window.lan['Deleting items...'],true);
          $.ajax({
            url: "/api",
            traditional : true,
            type: 'post',
            data:{action:'deleteCallItems',items:items},
            complete: function(data,str){
              if(data.status == 200){
                if(data.responseJSON && data.responseJSON.success){
                  $('#'+tid).bootstrapTable('refresh');
                  window.tip.show_pk("success",null,window.lan['Delete items success!']);
                }else{
                  window.tip.show_pk("danger",null,window.lan['Delete items failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
                }
              }else{
                window.tip.show_pk("danger",null,window.lan['Network error!']);
              }
            }});
        });
      })
    }
  }
  function emptyItem(tid){
    cf.createEmptyItemConfirm(function(){
      $('#myModal').modal().css({
        width: 'auto',
        backdrop:false
      });
      $('#myModal button[name=commit]').bind("click",function(){
        $('#myModal button[name=close]').trigger("click");
        window.tip.show_pk("info",60,window.lan['Emptying items...'],true);
        $.ajax({
          url: "/api",
          traditional : true,
          type: 'post',
          data:{action:'emptyCallItems'},
          complete: function(data,str){
            if(data.status == 200){
              if(data.responseJSON && data.responseJSON.success){
                $('#'+tid).bootstrapTable('refresh');
                window.tip.show_pk("success",null,window.lan['Empty items success!']);
              }else{
                window.tip.show_pk("danger",null,window.lan['Empty items failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
              }
            }else{
              window.tip.show_pk("danger",null,window.lan['Network error!']);
            }
          }});
      });
    })
  }
  function exportItem(tid){
    var rows=$('#'+tid).bootstrapTable('getSelections');
    if(rows.length==0) {
      window.tip.show_pk("warning", null, window.lan['You must select a item!']);
      return;
    }else {
      var items = [];
      for (var i = 0; i < rows.length; i++) {
        items.push(rows[i].uuid);
      }
      cf.createExportItems(function(){
        $('#myModal').modal().css({
          width: 'auto',
          backdrop: false
        });
        $("#myModal button[name=commit]").bind('click', function (e) {
          e.preventDefault();
          var exportType = 'csv';
          if($("#myModal input[name=exportType][value=txt]").prop('checked')){
            exportType = 'txt';
          }
          $('#myModal button[name=close]').trigger("click");
          window.tip.show_pk("info",60,window.lan['Exporting call tickets...'],true);
          $.ajax({
            url:'/api',
            type:"POST",
            traditional : true,
            data:{action:"exportCallTicket",downType:exportType,items:items},
            complete:function(data,str){
              if(data.status == 200){
                if(data.responseJSON && data.responseJSON.success){
                  $('#myModal a').attr('href', data.responseJSON.result.href);
                  $('#exportButton').click();
                  window.tip.show_pk("success",null,window.lan["Export call tickets success!"])
                }else{
                  window.tip.show_pk("danger",null,window.lan["Export call tickets error!"] + window.format.getErrmsg(data.responseJSON.errMsg))
                }
              }else{
                window.tip.show_pk("danger",null,window.lan["Network error!"])
              }
            }
          })
        });
      })
    }
  }
  return{
    createCallInfo:createCallInfo
  }
})