/**
 * Created by Rainc on 2015/7/22.
 */
var exec = require('child_process').exec;
var global = require('./controllers/global.js').global;
var os = require('os');
var fs = require('fs');
var async = require('async');

var log4js = require('log4js');
var logger = log4js.getLogger('DISABLECALL');
logger.setLevel('INFO');

var g_last_cpu_idle = -1;
var g_last_cpu_total = -1;

CPU_UTILIZATION = 0;     //cpu使用率，单位%
SERVER_MEMORY_UTILIZATION = 0;  //服务器内存使用率，单位%
DISK_UTILIZATION = 0;               //所在磁盘空间使用率，单位%
PROCESS_MEMORY_UTILIZATION = 0;   //程序内存使用率，单位%
LOADAVG_1M = 0                  //系统1分钟负载
LOADAVG_5M = 0                  //系统5分钟负载
LOADAVG_15M = 0                  //系统15分钟负载

function init(cb){
  //获取系统参数
  SYSTEM_UPTIME = global.localStaticToUTC(new Date().getTime() - Math.round(os.uptime()*1000));
  PROCESS_UPTIME = global.localStaticToUTC(new Date().getTime() - Math.round(process.uptime()*1000));
  PID = process.pid;
  TOTAL_MEMORY = os.totalmem() / (1024*1024);

  var _CPU_INFO = os.cpus();
  CPU_COUNT = _CPU_INFO.length;
  CPU_MODEL = _CPU_INFO[0].model;
  CPU_MHZ = _CPU_INFO[0].speed;

  var cpu_usage = os.loadavg();
  LOADAVG_1M = cpu_usage[0]?cpu_usage[0]:0;
  LOADAVG_5M = cpu_usage[1]?cpu_usage[1]:0;
  LOADAVG_15M = cpu_usage[2]?cpu_usage[2]:0;

  async.parallel([
    function(cb){
      //计算服务器内存使用率
      get_mem_usage(function(err,free){
        if(err){
          cb(err,0);
        }else{
          var serverMemoryUtilization = 100 - Math.round((free/TOTAL_MEMORY)*100);
          cb(null,serverMemoryUtilization);
        }
      });
    },
    function(cb){
      //计算空间使用率
      get_dir_mount_size(config.serverPath,function(err,size){
        if(err){
          cb(err,0)
        }else{
          cb(null,size);
        }
      })
    },
    function(cb){
      var command = "top -b -n 1 -p " + process.pid + " | grep node | awk '{print $10}'";
      exec(command, function(error, stdout, stderr) {
        if(error) {
          logger.error(error);
          cb(error);
        } else if(stderr) {
          logger.error(stderr);
          cb(stderr);
        } else {
          cb(null,stdout.split('\n')[0]);
        }
      });
    }
  ],function(err,data){
    if(err){
      cb(err)
    }else{
      CPU_UTILIZATION = get_cpu_utilization();
      SERVER_MEMORY_UTILIZATION = data[0];  //服务器内存使用率，单位%
      DISK_UTILIZATION = data[1];
      PROCESS_MEMORY_UTILIZATION = data[2];
      cb(null);
    }
  })
}

function get_cpu_utilization() {
  var cpus = os.cpus();
  var total = 0;
  var idle = 0;
  for(var index = 0; index < cpus.length; ++index) {
    var cpu = cpus[index];
    var cpu_total_times = 0;
    for(var key in cpu.times) {
      cpu_total_times += cpu.times[key];
    }
    total += cpu_total_times;
    idle += cpu.times.idle;
  }

  var utilization = 0;
  if(g_last_cpu_total >= 0) {
    var diff_total = total - g_last_cpu_total;
    var diff_idle = idle - g_last_cpu_idle;
    utilization = 100 - Math.round((diff_idle/diff_total) * 100);
  }
  g_last_cpu_idle = idle;
  g_last_cpu_total = total;
  return utilization;
}

//使用 df -P 命令统计文件目录所在磁盘的空间使用率
function get_dir_mount_size(dir,cb) {
  var command = "df -P "+dir;
  exec(command, function(error, stdout, stderr) {
    if(error) {
      logger.error(error);
      cb(error);
    } else if(stderr) {
      logger.error(stderr);
      cb(stderr);
    } else {
      var lines = stdout.split("\n");
      var info = {disk_used:0, disk_available:0};
      if(lines.length > 1) {
        var sline = lines[1].trim().replace(/\s+/g, ":");
        var aline = sline.split(":");
        if(aline[5]) {
          info.disk_used = (+aline[2]) / 1024; //kb
          info.disk_available = (+aline[3]) / 1024; //kb
        }
      }
      cb(null,parseInt(parseInt(info.disk_used)/(parseInt(info.disk_used) + parseInt(info.disk_available))*10000)/100);
    }
  });
}

function get_mem_usage(cb) {
  fs.readFile('/proc/meminfo', 'utf-8', function(error, data) {
    if(error) {
      logger.error(error);
      cb(error);
    } else {
      var free = 0;
      var lines = data.split("\n");
      for (var i = 0; i < lines.length; i++) {
        var aline = lines[i].split(':');
        if (!aline[1]) continue;
        var key = aline.shift();
        key = key.trim().toLowerCase();
        aline = aline[0].trim().replace(/\s+/g, ":");
        aline = aline.split(":");
        if (key == 'memfree' || key == 'buffers' || key == 'cached') {
          free += +aline[0] / 1024;
        }
      }
      cb(null,free);
    }
  });
}

exports.init = init;
